<?php

class ProductController extends AccountController
{
    public function index()
    {
        $data = array();
        if(CommonHelper::isAdmin())
        {
            $data['products'] = ProductModel::orderBy('create_date','desc')->paginate(15);
        }
        else
        {
            $data['products'] = ProductModel::where('id_user', Session::get('user')['id_user'])->orderBy('create_date','desc')->paginate(15);
        }

        return View::make('product.list', $data);

    }

    public function search()
    {
        if (Request::isMethod('post'))
        {
            $textSearch = Input::get('text_search');
            $data['isSearch'] = "Showing results for '".$textSearch."'";
            if(CommonHelper::isAdmin())
            {
                $data['products'] = ProductModel::where('subtitle', 'like', "%{$textSearch}%")->paginate(15);
            }
            else
            {
                $data['products'] = ProductModel::where('subtitle', 'like', "%{$textSearch}%")->where('id_user', Session::get('user')['id_user'])->paginate(15);
            }
            return View::make('product.list', $data);
        }
        return Redirect::to('product');
    }

    public function detail($id = 0)
    {
        $product = ProductModel::find($id);

        if(isset($product))
        {
            if(CommonHelper::isAdmin())
            {
                $data['product'] = $product;
            }
            else
            {
                if($product->id_user == Session::get('user')['id_user'])
                {
                    $data['product'] = $product;
                }
                else
                {
                    $data['noProduct'] = true;
                }
            }
        }
        else
        {
            $data['noProduct'] = true;
        }
        return View::make('product.detail', $data);
    }


	public function edit($id = 0)
	{
        $data = array('type' => 'edit');
        $data['header'] = 'Edit product';
        $data['action'] = URL::to('product/edit/'.$id);
        $data['categories'] = CategoryModel::where('isEnable',1)->get();
        $data['publishers'] = PublisherModel::all();
        if (Request::isMethod('post'))
        {
            $product = Input::get('data');
            $file = Input::file('file_epub');
            $fileDemo = Input::file('file_epub_demo');
            $image = Input::file('file_image');

            $dataValidate = array_merge($product, array('file_epub' => $file, 'file_image' => $image, 'file_epub_demo' => $fileDemo));
            $validator = Validator::make($dataValidate, $this->getValidationRule('edit'));
            if ($validator->fails())
            {
                $messages = $validator->messages();
                $data['formErrors'] = $messages;
                $product['link_product_charge'] = Input::get('old_file');
                $data['product'] = $product;
                return View::make('product.edit', $data);
            }

            $basefilename = Str::random(3)."_".date('Ymdhis');
            $destinationPath = Config::get('webservice.epub_folder');
            $folderImagePath = Config::get('webservice.image_folder');
            $preText = Config::get('webservice.pre_text_file');
            if(isset($file))
            {
                if($file->isValid())
                {
                    //Generate filename
                    $filename = $basefilename.".".$file->guessExtension();
                    $file->move($destinationPath, $filename);

                    //Prepare data
                    $product['link_product_charge'] = $filename;

                    //Delete old file
                    $old_file = Input::get('old_file');
                    if(file_exists($destinationPath.$old_file))
                    {
                        unlink($destinationPath.$old_file);
                    }
                }
                else
                {
                    $data['singleError'] = 'Something wrong!';
                    $product['link_product_charge'] = Input::get('old_file');
                    $product['link_image'] = Input::get('old_file_image');
                    $data['product'] = $product;

                    return View::make('product.edit', $data);
                }
            }

            if(isset($fileDemo))
            {
                if($fileDemo->isValid())
                {                    
                    //Generate filename
                    $filenameDemo = $preText.$basefilename.".".$fileDemo->guessExtension();                    
                    $fileDemo->move($destinationPath, $filenameDemo);

                    $epub = new EpubHelper($destinationPath.$filenameDemo);
                    if($epub->checkDRM())
                    {
                        //Remove file
                        if(file_exists($destinationPath.$filenameDemo))
                        {
                            unlink($destinationPath.$filenameDemo);
                        }
                        $data['singleError'] = 'File demo is ecrypted! DRM';
                        $product['link_product_charge'] = Input::get('old_file');
                        $product['link_image'] = Input::get('old_file_image');
                        $data['product'] = $product;

                        return View::make('product.edit', $data);
                    }

                    //Prepare data
                    $product['link_product_demo'] = $filenameDemo;

                    //Delete old file
                    $old_file_demo = Input::get('old_file_demo');
                    if(file_exists($destinationPath.$old_file_demo))
                    {
                        unlink($destinationPath.$old_file_demo);
                    }
                }
                else
                {
                    $data['singleError'] = 'Something wrong!';
                    $product['link_product_charge'] = Input::get('old_file');
                    $product['link_image'] = Input::get('old_file_image');                    
                    $data['product'] = $product;

                    return View::make('product.edit', $data);
                }
            }

            if(isset($image))
            {
                if($image->isValid())
                {
                    //Save image
                    $imgText = Config::get('webservice.img_text_file');
                    $imagefilename = $imgText.$basefilename.".".$image->guessExtension();
                    $image->move($folderImagePath, $imagefilename);

                    //Prepare data
                    $product['link_image'] = $imagefilename;

                    //Delete old file
                    $old_file_image = Input::get('old_file');
                    if(file_exists($folderImagePath.$old_file_image))
                    {
                        unlink($folderImagePath.$old_file_image);
                    }
                }
                else
                {
                    $data['singleError'] = 'Something wrong!';
                    $product['link_product_charge'] = Input::get('old_file');
                    $product['link_image'] = Input::get('old_file_image');
                    $data['product'] = $product;

                    return View::make('product.edit', $data);
                }
            }

            ProductModel::where('id_product', $id)->update($product);
            return Redirect::to('product');
        }
        else
        {
            $product = ProductModel::find($id);

            if(isset($product))
            {
                if(CommonHelper::isAdmin())
                {
                    $data['product'] = $product->toArray();
                }
                else
                {
                    if($product->id_user == Session::get('user')['id_user'])
                    {
                        $data['product'] = $product->toArray();
                    }
                    else
                    {
                        $data['noProduct'] = true;
                    }
                }
            }
            else
            {
                $data['noProduct'] = true;
            }
            return View::make('product.edit', $data);
        }
	}

    public function create()
    {
        $data = array('type' => 'new');
        $data['header'] = 'Create product';
        $data['action'] = URL::to('product/create');
        $data['categories'] = CategoryModel::where('isEnable',1)->get();
        $data['publishers'] = PublisherModel::all();
        $preText = Config::get('webservice.pre_text_file');
        if (Request::isMethod('post'))
        {
            $product = Input::get('data');
            $file = Input::file('file_epub');
            $image = Input::file('file_image');
            $fileDemo = Input::file('file_epub_demo');
            $dataValidate = array_merge($product, array('file_epub' => $file, 'file_image' => $image, 'file_epub_demo' => $fileDemo));

            $validator = Validator::make($dataValidate, $this->getValidationRule('create'));
            if ($validator->fails())
            {
                $messages = $validator->messages();
                $data['formErrors'] = $messages;
                $data['product'] = $product;
                return View::make('product.edit', $data);
            }

            if($file->isValid() && $image->isValid())
            {
                $destinationPath = Config::get('webservice.epub_folder');
                $folderImagePath = Config::get('webservice.image_folder');
                //Generate filename
                $basefilename = Str::random(3)."_".date('Ymdhis');
                $filename = $basefilename.".".$file->guessExtension();
                $file->move($destinationPath, $filename);

                //Save image
                $imgText = Config::get('webservice.img_text_file');
                $imagefilename = $imgText.$basefilename.".".$image->guessExtension();
                $image->move($folderImagePath, $imagefilename);

                if(isset($fileDemo) && $fileDemo->isValid())
                {
                    $filenameDemo = $preText.$basefilename.".".$fileDemo->guessExtension();
                    $fileDemo->move($destinationPath, $filenameDemo);
                }
                else
                {
                    $epub = EpubHelper::initInstance($destinationPath.$filename);
                    $epub->getPreviewEpub();
                }


                //Prepare data
                $product['id_user'] = Session::get('user')['id_user'];
                $product['link_product_demo'] = $preText.$filename;
                $product['link_product_charge'] = $filename;
                $product['link_image'] = $imagefilename;
                $product['create_date'] = date('Y-m-d h:i:s');
                ProductModel::insert($product);
                return Redirect::to('product');
            }
            else
            {
                $data['singleError'] = 'Something wrong!';
                $data['product'] = $product;
                return View::make('product.edit', $data);
            }
        }
        else
        {
            return View::make('product.edit', $data);
        }
    }

    public function delete($id)
    {
    	$product = ProductModel::find($id);
        if(!CommonHelper::isAdmin() && $product->id_user != Session::get('user')['id_user'])
        {
            return Redirect::to('product');
        }

    	$data = array();
        if(isset($product))
        {
            $data['product'] = $product;
        }
        else
        {
            return Redirect::to('product');
        }
    	return View::make('product.delete', $data);
    }

    public function doDelete($id)
    {
    	$product = ProductModel::find($id);
        if(!CommonHelper::isAdmin() && $product->id_user != Session::get('user')['id_user'])
        {
            return Redirect::to('product');
        }
        if(isset($product))
        {
            $destinationPath = Config::get('webservice.epub_folder');
            $folderImagePath = Config::get('webservice.image_folder');
            if(file_exists($destinationPath.$product->link_product_charge))
            {
                unlink($destinationPath.$product->link_product_charge);
            }
            if(file_exists($destinationPath.$product->link_product_demo))
            {
                unlink($destinationPath.$product->link_product_demo);
            }
            if(file_exists($folderImagePath.$product->link_image))
            {
                unlink($folderImagePath.$product->link_image);
            }
    		$product->delete();
            PromotionModel::where('id_product', $id)->delete();
        }
        return Redirect::to('product');
    }

    public function download($filename)
    {
        $destinationPath = Config::get('webservice.epub_folder');
        $filePath = $destinationPath.$filename;
        if(file_exists($filePath))
        {
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-type: application/epub+zip");
            header("Content-Disposition: attachment; filename=".$filename);
            header("Content-Type: application/octet-stream");
            header("Content-Transfer-Encoding: binary");
            readfile($filePath);
        }
        else
        {
            return;
        }
    }    

    private function getValidationRule($type)
    {
        if($type == 'create')
        {
            return  array(
                'subtitle' => 'required',
                'author' => 'required',
                'id_category' => 'required',
                'id_publisher' => 'required',
                'price' => 'required|numeric|min:0',
                'summer' => 'required',
                'file_epub' => 'required|mimes:epub',
                'file_epub_demo' => 'mimes:epub',
                'file_image' => 'required|image',
                'isSpecailproduct' => '',
                'time_for_specail_product' => 'required_if:isSpecailproduct,1',
            );
        }

        if($type == 'edit')
        {
            return  array(
                'subtitle' => 'required',
                'author' => 'required',
                'id_category' => 'required',
                'id_publisher' => 'required',
                'price' => 'required|numeric|min:0',
                'summer' => 'required',
                'isSpecailproduct' => '',
                'numPayment' => 'required|numeric',
                'file_epub' => 'mimes:epub',
                'file_epub_demo' => 'mimes:epub',
                'file_image' => 'image',
                'time_for_specail_product' => 'required_if:isSpecailproduct,1',
            );
        }
    }

}
