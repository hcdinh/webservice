<?php

class APIController extends APIBaseController
{
	public function signin()
	{
		$userModel = UserModel::initInstance();
		$email = $this->data['email'];

		if(!$userModel->checkEmail($email))
		{            
        	//Prepare data
            $newDevice = array();
            $newDevice['device_name'] = $this->data['device_name'];

            unset($this->data['device_name']);
            $this->data['create_date'] = date('Y-m-d h:i:s');
			$this->data['password'] = md5($this->data['password']);
			$this->data['bored_date'] = date('Y-m-d h:i:s',strtotime($this->data['bored_date']));
			$this->data['deposit'] = 0;
			$this->data['permission'] = 0;
			$this->data['isEnable'] = 1;
			$this->data['numDevice'] = 1;
			$newUserId = UserModel::insertGetId($this->data);

			$newDevice['id_user'] = $newUserId;
			$newDevice['create_date'] = date('Y-m-d h:i:s');
			$newDevice['isEnable'] = 1;
			DeviceModel::insert($newDevice);

			$this->dataResponse['error_code'] = "00";
			return $this->returnResponse();
		}
		else
		{
			$this->dataResponse['error_code'] = "01";
			$this->dataResponse['description']['en'] = Config::get('message.en.email_existed');
		    $this->dataResponse['description']['vi'] = Config::get('message.vi.email_existed');
			return $this->returnResponse();
		}
	}

	public function login()
	{
		$userModel = UserModel::initInstance();
		$deviceModel = DeviceModel::initInstance();		

        //Check user login
        $dataUser = $userModel->checkUserLoginAPI($this->data['email'],$this->data['password']);
		if($dataUser != FALSE)
		{
			//Information for login right
			//Check device
			if($deviceModel->checkDeviceUser($dataUser['id_user'], $dataUser['numDevice'], $this->data['device_name']))
			{				
				$token = md5(date('Ymdhis').$dataUser['email']);
				$tokenId = md5($token.$this->data['device_name']);
				$sessionId = time();

				$dataUpdate = array();
				$dataUpdate['idSession'] = $sessionId;
				$dataUpdate['idToken'] = $tokenId;
				$dataUpdate['lastest_login_date'] = date('Y-m-d h:i:s');
				$userModel->where('id_user', $dataUser['id_user'])->update($dataUpdate);
				$this->dataResponse['error_code'] = "00";
	     		$this->dataResponse['items']['token'] = $token;
				return $this->returnResponse();

			}
			else
			{
				$this->dataResponse['error_code'] = "01";
				$this->dataResponse['description']['en'] = Config::get('message.en.full_device');
			    $this->dataResponse['description']['vi'] = Config::get('message.vi.full_device');
			    return $this->returnResponse();
			}
		}
		else
		{
			$this->dataResponse['error_code'] = "01";
			$this->dataResponse['description']['en'] = Config::get('message.en.login_fail');
		    $this->dataResponse['description']['vi'] = Config::get('message.vi.login_fail');
		    return $this->returnResponse();
		}
	}

	public function logout()
	{
		$datatemp = file_get_contents('php://input');
		$datatemp = CommonHelper::decrypt($datatemp);
        $datatemp = json_decode($datatemp, true);
        if(json_last_error() != JSON_ERROR_NONE)
        {
        	$this->dataResponse['error_code'] = "00";	     	
        	return $this->returnResponse();
        }
        else
        {
        	$this->data = $datatemp;
        	if(count($datatemp) == 0)
			{
				$this->dataResponse['error_code'] = "00";	     		
				return $this->returnResponse();
			}
        }
        if(!isset($this->data['token']) || !isset($this->data['device_name']))
    	{
    		$this->dataResponse['error_code'] = "00";	     	
        	return $this->returnResponse();
    	}
    	$token = md5($this->data['token'].$this->data['device_name']);
    	$userModel = UserModel::initInstance();
    	$this->dataUserLogin = $userModel->getUserByToken($token);

    	if($this->dataUserLogin==FALSE)
    	{
    		$this->dataResponse['error_code'] = "00";	     	
        	return $this->returnResponse();	
    	}

		$dataUpdate = array();
		$dataUpdate['idSession'] = "";
		$dataUpdate['idToken'] = "";
		UserModel::where('id_user', $this->dataUserLogin['id_user'])->update($dataUpdate);
		$this->dataResponse['error_code'] = "00"; 		
		return $this->returnResponse();
	}

	public function updatePassword()
	{
		$userModel = UserModel::initInstance();
		$oldpass = md5($this->data['oldpass']);
		$data = $userModel->where('id_user', $this->dataUserLogin['id_user'])->where('password', $oldpass);
		if(count($data->get()->toArray()) > 0)
		{
			$data->update(array('password' => md5($this->data['newpass'])));
			$this->dataResponse['error_code'] = "00"; 		
			return $this->returnResponse();
		}
		$this->dataResponse['error_code'] = "01"; 		
		$this->dataResponse['description']['en'] = Config::get('message.en.wrong_old_pass');
		$this->dataResponse['description']['vi'] = Config::get('message.vi.wrong_old_pass');
		return $this->returnResponse();
	}

	public function checkLogin()
	{
		$token = md5($this->data['token'].$this->data['device_name']);
		$userModel = UserModel::initInstance();
		$userCheck = $userModel->getUserByToken($token);
		if($userCheck !=  FALSE)
		{
			$dataUpdate = array();
			$dataUpdate['idSession'] = time();
			$dataUpdate['lastest_login_date'] = date('Y-m-d h:i:s');
			$userModel->where('id_user', $userCheck['id_user'])->update($dataUpdate);
			$this->dataResponse['error_code'] = "00"; 		
			return $this->returnResponse();
		}
		$this->dataResponse['error_code'] = "01"; 		
		return $this->returnResponse();
	}

	public function getListProduct1()
	{
		$productModel = ProductModel::initInstance();
		$filter = array();
		$filter['type'] = $this->data['kind'];
		if($this->data['kind'] == 1)
		{
			$filter['id_category'] = $this->data['idcategory'];
		}
		if($this->data['kind'] == 4)
		{
			$filter['id_user'] = $this->dataUserLogin['id_user'];			
		}
		$numItems = 10;
		$page = 0;
		$totalPage = 0;
		if(isset($this->data['numitems']))
		{
			$numItems = $this->data['numitems'];
		}
		if(isset($this->data['page']))
		{
			$page = $this->data['page'] - 1;
		}		
		$this->dataResponse['error_code'] = "00";		
		$this->dataResponse['items'] = $productModel->getListProduct1($filter, $totalPage, $numItems, $page);		
		if($page == 0)
		{
			$this->dataResponse['numpage'] = $totalPage;
		}
		return $this->returnResponse();
	}

	public function getListProduct2()
	{
		$productModel = ProductModel::initInstance();
		$page = 0;
		if(isset($this->data['page']))
		{
			$page = $this->data['page'] - 1;
		}
		$this->dataResponse['error_code'] = "00";		
		$this->dataResponse['items'] = $productModel->getListProduct2($this->data['kind'], $totalPage, $page);		
		if($page == 0)
		{
			$this->dataResponse['numpage'] = $totalPage;
		}
		return $this->returnResponse();

	}

	public function getInfoProduct()
	{
		$paymentModel = PaymentModel::initInstance();
		$productId = $this->data['id_product'];				
		$product = ProductModel::where('id_product', $productId)->first();
		if(isset($product))
		{
			$this->dataResponse['error_code'] = "00";
			$this->dataResponse['id_product'] = $productId;
			$this->dataResponse['items']['id_category'] = $product->id_category;
			$this->dataResponse['items']['publisher_name'] = $product->publisher->publisher_name;
			$this->dataResponse['items']['author'] = $product->author;
			$this->dataResponse['items']['subtitle'] = $product->subtitle;
			$this->dataResponse['items']['price'] = $product->price;
			$this->dataResponse['items']['summer'] = $product->summer;
			$this->dataResponse['items']['numPayment'] = $product->numPayment;
			$this->dataResponse['items']['link_image'] = URL::to('image/'.$product->link_image);
			$check = $paymentModel->checkBuyProduct($this->dataUserLogin['id_user'], $productId);
			if($check == NOT_BUY || $check == BUY_PROMOTION_EXPIRED)
			{
				$this->dataResponse['items']['ispayed'] = NO;	
			}
			else
			{
				$this->dataResponse['items']['ispayed'] = YES;
			}	
			return $this->returnResponse();		
		}
		$this->dataResponse['error_code'] = "01"; 		
		$this->dataResponse['description']['en'] = Config::get('message.en.no_product');
		$this->dataResponse['description']['vi'] = Config::get('message.vi.no_product');
		return $this->returnResponse();
		
	}

	public function checkBuyProduct()
	{
		$paymentModel = PaymentModel::initInstance();
		$check = $paymentModel->checkBuyProduct($this->dataUserLogin['id_user'], $this->data['id_product']);
		if($check == NOT_BUY)
		{
			$this->dataResponse['error_code'] = "01"; 								
		}		
		elseif($check == BUY_FOREVER)
		{
			$this->dataResponse['error_code'] = "00"; 							
		}
		else
		{
			$this->dataResponse['error_code'] = "02";
		}
		return $this->returnResponse();
	}

	public function checkExpiredProduct()
	{
		$paymentModel = PaymentModel::initInstance();
		$check = $paymentModel->checkBuyProduct($this->dataUserLogin['id_user'], $this->data['id_product']);
		if($check == NOT_BUY || $check == BUY_PROMOTION_EXPIRED)
		{
			$this->dataResponse['error_code'] = "01"; 								
		}		
		elseif($check == BUY_FOREVER || $check == BUY_PROMOTION)
		{
			$this->dataResponse['error_code'] = "00"; 							
		}
		return $this->returnResponse();
	}

	public function buyProduct()
	{
		$user = $this->dataUserLogin;
		$this->dataResponse['items'] = array('id_product' => $this->data['id_product']);
		$paymentModel = PaymentModel::initInstance();
		$check = $paymentModel->checkBuyProduct($user['id_user'], $this->data['id_product']);
		if($check != NOT_BUY)			
		{
			if($check == BUY_PROMOTION_EXPIRED)
			{
				$paymentModel->where('id_user', $user['id_user'])
								->where('id_product', $this->data['id_product'])
								->delete();
			}
			else
			{
				$this->dataResponse['error_code'] = "05";
				return $this->returnResponse();		
			}			
		}
		$product = ProductModel::find($this->data['id_product']);		
		if(isset($product))
		{
			$dataPayment = array();
			$dataPayment['id_user'] = $user['id_user'];
			$dataPayment['id_product'] = $product->id_product;
			$dataPayment['create_date'] = date('Y-m-d h:i:s');

			$dataUpdateUser = array();
			if($product->isSpecailproduct == NO)
			{					
				if($user['havePromotionUse'] == YES && strtotime($user['start_date']) < time() && time() < strtotime($user['end_date']))
				{					
					$dataPayment['price'] = 0;
					$dataPayment['isPromotion'] = YES;
				}
				else
				{					
					$dataPayment['price'] = $product->price;					
					$dataPayment['isPromotion'] = NO;
				}				
			}
			else
			{
				$dataPayment['price'] = $product->price;					
				$dataPayment['isPromotion'] = NO;

				if(strtotime($user['end_date']) < time())
				{
					$dataUpdateUser['havePromotionUse'] = YES;
					$dataUpdateUser['start_date'] = date('Y-m-d h:i:s');
					$dataUpdateUser['end_date'] = date('Y-m-d h:i:s', strtotime("+".$product->time_for_specail_product." day",time()));					
				}
				else
				{					
					$dataUpdateUser['havePromotionUse'] = YES;
					$dataUpdateUser['end_date'] = date('Y-m-d h:i:s', strtotime("+".$product->time_for_specail_product." day",strtotime($user['end_date'])));					
				}	
			}
			$dataUpdateUser['deposit'] = $user['deposit'] - $dataPayment['price'];
			if($dataUpdateUser['deposit'] < 0)
			{
				$this->dataResponse['error_code'] = "04";
				return $this->returnResponse();
			}
			UserModel::where('id_user', $user['id_user'])->update($dataUpdateUser);
			PaymentModel::insert($dataPayment);
			$product->numPayment++;
			$product->save();

			//Send mail
			$data['product'] = $product;
			$data['payment'] = $dataPayment;
			$data['user'] = $user;

			Mail::send('email', $data, function($message) use ($product)
			{			
			  $message->from('admin@webservice.com', 'Webservice');
			  $message->to($product->user->email, $product->user->user_name)->subject('Có một user đã mua sản phẩm');
			});			

			$this->dataResponse['error_code'] = "00";
			return $this->returnResponse();
		}
		$this->dataResponse['error_code'] = "01"; 		
		$this->dataResponse['description']['en'] = Config::get('message.en.no_product');
		$this->dataResponse['description']['vi'] = Config::get('message.vi.no_product');
		return $this->returnResponse();		
	}

	public function downloadEpubDirect()
	{
		$code = Input::get('data');
		if(is_null($code))
		{
			return NULL;
		}
		$action = CommonHelper::getCurrentAction();		
		$datatemp = CommonHelper::decrypt($code);
		$datatemp = json_decode($datatemp, true);
		if(json_last_error() != JSON_ERROR_NONE)
        {
        	return NULL;
        }
        else
        {        	
        	$this->data = $datatemp;
        	if(count($datatemp) == 0)
			{
				return NULL;
			}
        }      
 
    	$rule = $this->getRuleAction($action);
    	if($rule != FALSE)
    	{
    		$resultValidate = CommonHelper::validateDataAPI($this->data, $rule);
			if ($resultValidate != 1)
            {
                return NULL;
            }
    	}
    	if(!isset($this->data['token']))
		{
			return NULL;
		}
		if(!isset($this->data['device_name']))
		{
			return NULL;
		}
		$token = md5($this->data['token'].$this->data['device_name']);
		$userModel = UserModel::initInstance();
		$this->dataUserLogin = $userModel->getUserByToken($token);
		if($this->dataUserLogin!=FALSE)
		{
			//Check session
			$maxTimeSession = Config::get('webservice.time_session');
			$timeSession = ((int)time() - (int)$this->dataUserLogin['idSession'])/60;
			$timeLogin = ((int)time() - (int)strtotime(date('Y-m-d'), strtotime($this->dataUserLogin['lastest_login_date'])))/60;
			if(max($timeSession, $timeLogin) > $maxTimeSession)
			{
				//Logout
				$dataUpdate = array();
				$dataUpdate['idSession'] = "";
				$dataUpdate['idToken'] = "";
				$userModel->where('id_user', $this->dataUserLogin['id_user'])->update($dataUpdate);
				return NULL;
			}
		}
		else
		{
			return NULL;
		}
    	return $this->downloadEpub();


	}

	public function downloadEpub()
	{
		$product = ProductModel::find($this->data['id_product']);			
		if(isset($product))
		{
			$destinationPath = Config::get('webservice.epub_folder');
			if($this->data['kind_product'] == 0)
			{
				$filename = $product->link_product_demo;
			}
			elseif($this->data['kind_product'] == 1)				
			{
				$paymentModel = PaymentModel::initInstance();
				$check = $paymentModel->checkBuyProduct($this->dataUserLogin['id_user'], $this->data['id_product']);
				if($check == NOT_BUY || $check == BUY_PROMOTION_EXPIRED)
				{
					return NULL;
				}		
				elseif($check == BUY_FOREVER || $check == BUY_PROMOTION)
				{
					$payment = $paymentModel->where('id_user', $this->dataUserLogin['id_user'])
									->where('id_product', $this->data['id_product'])
									->first();
					if($payment->canDownload == YES)
					{
						$filename = $product->link_product_charge;
					}
					else
					{
						return NULL;
					}
					
				}
			}	
			else
			{
				return NULL;
			}
			$filePath = $destinationPath.$filename;	        
	        if(file_exists($filePath))
	        {
	            header("Pragma: public");
	            header("Expires: 0");
	            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	            header("Cache-Control: public");
	            header("Content-Description: File Transfer");
	            header("Content-type: application/epub+zip");
	            header("Content-Disposition: attachment; filename=".$filename);
	            header("Content-Type: application/octet-stream");
	            header("Content-Transfer-Encoding: binary");
	            header("Accept-Ranges: bytes");
	            readfile($filePath);
	        }
	        else
	        {
	            return NULL;
	        }
			
		}
		return NULL;
	}

	public function getListDevice()
	{
		$listDevice = DeviceModel::where('id_user', $this->dataUserLogin['id_user'])
								->where('isEnable', YES)->get()->toArray();
		$this->dataResponse['error_code'] = "00";
		$this->dataResponse['items'] = $listDevice;
		return $this->returnResponse();		
	}

	public function getListPayment()
	{
		$listPayment = PaymentModel::where('id_user', $this->dataUserLogin['id_user'])
									->orderBy('create_date', 'desc')->get();
		$data = array();
		foreach ($listPayment as $payment) 
		{
			$temp = array();
			$temp['subtitle'] = $payment->product->subtitle;
			$temp['create_date'] = $payment->create_date;
			$temp['price'] = $payment->price;
			$temp['isPromotion'] = $payment->isPromotion;
			$data[] = $temp;			
		}

		$this->dataResponse['error_code'] = "00";
		$this->dataResponse['items'] = $data;
		return $this->returnResponse();		
	}

	public function deleteDevice()
	{
		$device = DeviceModel::where('id_device', $this->data['id_device'])
						->where('id_user', $this->dataUserLogin['id_user'])
						->where('device_name', $this->data['device_name_delete'])
						->first();
		if(isset($device))
		{
			$device->delete();			
			$user = UserModel::find($this->dataUserLogin['id_user']);
			if($user->numDevice > 0)
			{
				$user->numDevice --;	
			}
			$user->save();
			
			$this->dataResponse['error_code'] = "00";
		}
		else
		{
			$this->dataResponse['error_code'] = "01";
		}
		
		$this->dataResponse['items'] = array('id_device' => $this->data['id_device']);
		return $this->returnResponse();	
	}
}
