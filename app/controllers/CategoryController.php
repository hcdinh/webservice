<?php

class CategoryController extends AccountController 
{
    public function __construct()
    {
        $this->beforeFilter(function()
        {
            $crAction = CommonHelper::getCurrentAction();
            $arrayOnlyAdmin = array('edit', 'create', 'delete', 'doDelete');
            if(!CommonHelper::isAdmin())
            {
                if(in_array($crAction, $arrayOnlyAdmin))
                {
                    return Redirect::to('category');
                }
            }
        });
    }

    public function index()
    {
        $data = array();
        $data['categories'] = CategoryModel::paginate(15);
        return View::make('category.list', $data);

    }

    public function search()
    {
        if (Request::isMethod('post'))
        {
            $textSearch = Input::get('text_search');
            $data['isSearch'] = "Showing results for '".$textSearch."'";            
            $data['categories'] = CategoryModel::where('category_name', 'like', "%{$textSearch}%")->paginate(15);
            return View::make('category.list', $data);
        }        
        return Redirect::to('category');
    }


	public function edit($id = 0)
	{
        $data = array('type' => 'edit');
        $data['header'] = 'Edit category';
        $data['action'] = URL::to('category/edit/'.$id);
        if (Request::isMethod('post'))
        {
            $category = Input::get('data');
            $validator = Validator::make($category, $this->getValidationRule());
            if ($validator->fails())
            {
                $messages = $validator->messages();
                $data['formErrors'] = $messages;
                $data['category'] = $category;
                return View::make('category.edit', $data);
            }

            CategoryModel::where('id_category', $id)->update($category);
            return Redirect::to('category');
        }
        else
        {
            $category = CategoryModel::find($id);
            if(isset($category))
            {
                $data['category'] = $category->toArray();
            }
            else
            {
                $data['noCategory'] = true;
            }
            return View::make('category.edit', $data);
        }  
	}

    public function create()
    {
        $data = array('type' => 'new');
        $data['header'] = 'Create category';
        $data['action'] = URL::to('category/create');
        if (Request::isMethod('post'))
        {
            $category = Input::get('data');
            $validator = Validator::make($category, $this->getValidationRule());
            if ($validator->fails())
            {
                $messages = $validator->messages();
                $data['formErrors'] = $messages;
                $data['category'] = $category;
                return View::make('category.edit', $data);
            }

            CategoryModel::insert($category);
            return Redirect::to('category');
        }
        else
        {
            return View::make('category.edit', $data);
        }
    }

    public function delete($id)
    {
    	$category = CategoryModel::find($id);
    	$data = array();
        if(isset($category))
        {
            $data['category'] = $category;
        }
        else
        {
            return Redirect::to('category');
        }
    	return View::make('category.delete', $data);
    }

    public function doDelete($id)
    {
    	$category = CategoryModel::find($id);    	
        if(isset($category))
        {
    		$category->delete();        
        }
        return Redirect::to('category');
    }

    private function getValidationRule()
    {
        return  array(
            'category_name' => 'required',
        );    
    }

}
