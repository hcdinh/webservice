<?php

class APIBaseController extends BaseController
{
	protected $data;
	protected $dataResponse;
	protected $listNoAuthAPI;
	protected $listNotCheckAPI;
	protected $dataUserLogin;

	public function __construct()
    {
    	$this->dataResponse = array();
    	$this->listNoAuthAPI = array('signin', 'login', 'checkLogin');
    	$this->listNotCheckAPI = array('logout', 'downloadEpubDirect');
    	$this->beforeFilter(function()
        {
        	$action = CommonHelper::getCurrentAction();
        	if(!in_array($action, $this->listNotCheckAPI))
        	{
        		$datatemp = file_get_contents('php://input');
        		$datatemp = CommonHelper::decrypt($datatemp);
		        $datatemp = json_decode($datatemp, true);
		        if(json_last_error() != JSON_ERROR_NONE)
		        {
		        	$this->dataResponse['error_code'] = "99";
			     	$this->dataResponse['description']['en'] = Config::get('message.en.json_format');
			     	$this->dataResponse['description']['vi'] = Config::get('message.vi.json_format');
		        	return $this->returnResponse();
		        }
		        else
		        {
		        	$this->data = $datatemp;
		        	if(count($datatemp) == 0)
					{
						$this->dataResponse['error_code'] = "99";
			     		$this->dataResponse['description']['en'] = Config::get('message.en.no_data');
			     		$this->dataResponse['description']['vi'] = Config::get('message.vi.no_data');
						return $this->returnResponse();
					}
		        }	        

		        //Validate data
	        	$rule = $this->getRuleAction($action);
	        	if($rule != FALSE)
	        	{
	        		$resultValidate = CommonHelper::validateDataAPI($this->data, $rule);
					if ($resultValidate != 1)
		            {
		                $this->dataResponse['error_code'] = "01";
		         		$this->dataResponse['description'] = $resultValidate;
						return $this->returnResponse();
		            }
	        	}

	            //Check auth
		        if(!in_array($action, $this->listNoAuthAPI))
		        {
		        	if(!isset($this->data['token']))
		        	{
		        		$this->dataResponse['error_code'] = "98";
			     		$this->dataResponse['description']['en'] = Config::get('message.en.no_token');
			     		$this->dataResponse['description']['vi'] = Config::get('message.vi.no_token');
						return $this->returnResponse();
		        	}
		        	if(!isset($this->data['device_name']))
		        	{
		        		$this->dataResponse['error_code'] = "97";
			     		$this->dataResponse['description']['en'] = Config::get('message.en.no_device');
			     		$this->dataResponse['description']['vi'] = Config::get('message.vi.no_device');
						return $this->returnResponse();
		        	}
		        	$token = md5($this->data['token'].$this->data['device_name']);
		        	$userModel = UserModel::initInstance();
		        	$this->dataUserLogin = $userModel->getUserByToken($token);
		        	if($this->dataUserLogin!=FALSE)
		        	{
		        		//Check session
		        		$maxTimeSession = Config::get('webservice.time_session');
		        		$timeSession = ((int)time() - (int)$this->dataUserLogin['idSession'])/60;
		        		$timeLogin = ((int)time() - (int)strtotime($this->dataUserLogin['lastest_login_date']))/60;
		        		
		        		if(max($timeSession, $timeLogin) > $maxTimeSession)
						{
		        			//Logout
		        			$dataUpdate = array();
							$dataUpdate['idSession'] = "";
							$dataUpdate['idToken'] = "";
							$userModel->where('id_user', $this->dataUserLogin['id_user'])->update($dataUpdate);


		        			$this->dataResponse['error_code'] = "02";
				     		$this->dataResponse['description']['en'] = Config::get('message.en.session_timeout');
				     		$this->dataResponse['description']['vi'] = Config::get('message.vi.session_timeout');
							return $this->returnResponse();
		        		}
		        	}
		        	else
		        	{
		        		$this->dataResponse['error_code'] = "03";
			     		$this->dataResponse['description']['en'] = Config::get('message.en.wrong_token');
			     		$this->dataResponse['description']['vi'] = Config::get('message.vi.wrong_token');
						return $this->returnResponse();
		        	}
		        }	
        	}
	        
	    });
    }

    public function returnResponse()
    {
    	$data = CommonHelper::encrypt(json_encode($this->dataResponse));
    	return Response::make($data, 200)->header('Content-Type','text\html');
    }

    protected function getRuleAction($action)
    {
    	$rule = FALSE;
    	switch ($action)
    	{
    		case 'signin':
    			$rule = array(
					'email' => 'required|email',
					'password' => 'required|min:6|max:255',
					'user_name' => 'required',
					'bored_date' => 'required|date',
					'sex' => 'required|min:0|max:1|integer',
					'device_name' => 'required'
				);
    			break;
    		case 'login':
    			$rule = array(
					'email' => 'required|email',
					'password' => 'required',
					'device_name' => 'required'
				);
    			break;
    		case 'updatePassword':
    			$rule = array(
					'oldpass' => 'required',
					'newpass' => 'required|min:6|max:255',
					'token' => 'required',
					'device_name' => 'required'
				);
    			break;
    		case 'checkLogin':
    		case 'getListDevice':
    		case 'getListPayment':
    			$rule = array(
					'token' => 'required',
					'device_name' => 'required'
				);
    			break;
    		case 'getInfoProduct':
    			$rule = array(
					'token' => 'required',
					'device_name' => 'required',
					'id_product' => 'required'
				);
    			break;
    		case 'getListProduct1':
    			$rule = array(
					'token' => 'required',
					'device_name' => 'required',
					'kind' => 'required|min:0|max:4|integer',
					'idcategory' => 'required_if:kind,1|integer',
					'numitems' => 'integer',
					'page' => 'integer'
				);
    			break;

    		case 'getListProduct2':
    			$rule = array(
					'token' => 'required',
					'device_name' => 'required',
					'kind' => 'required|min:0|max:1|integer',
					'page' => 'integer|max:2|min:1'
				);
    			break;
    		case 'checkBuyProduct':
    		case 'checkExpiredProduct':
    		case 'buyProduct':
    			$rule = array(
					'token' => 'required',
					'device_name' => 'required',					
					'id_product' => 'required|integer'
				);
    			break;
    		case 'downloadEpub':
    		case 'downloadEpubDirect':
    			$rule = array(
					'token' => 'required',
					'device_name' => 'required',					
					'id_product' => 'required|integer',
					'kind_product' => 'required|integer|min:0|max:1'
				);
    			break;
    		case 'deleteDevice':
    			$rule = array(
					'token' => 'required',
					'device_name' => 'required',					
					'id_device' => 'required|integer',					
					'device_name_delete' => 'required'
				);
    			break;
    	}

    	return $rule;
    }
}
