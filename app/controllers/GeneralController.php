<?php

class GeneralController extends BaseController 
{
	public function __construct()
    {
        $this->beforeFilter(function()
        {
        	$userId = Cookie::get('webservice');
        	if(!is_null($userId) || !is_null(Session::get('user', null)))
        	{
        		$action = CommonHelper::getCurrentAction();
        		if($action != 'image')
        		{
        			return Redirect::to('category');		
        		}
        		
        	}
        });
    }


	public function login()
	{
		if (Request::isMethod('post'))
		{
			$email = Input::get('email');
			$password = Input::get('password');
			$userModel = UserModel::initInstance();
			$data = $userModel->checkUserLogin($email,$password);
			if($data != FALSE)
			{
				//Save login user
				Session::put('user', $data);

				$rememberMe =  Input::get('remember');
				if($rememberMe != 0)
				{
					$cookie = Cookie::forever('webservice', $data['id_user']);
					return Redirect::to('category')->withCookie($cookie);	
				}
				UserModel::where('id_user', $data['id_user'])->update(array('lastest_login_date' => date('Y-m-d h:i:s')));
				//Redirect to homepage	
				return Redirect::to('category');	
			}
			else
			{
				Session::flash('error', 'Email or password isn\'t right');				
				return View::make('general.login');
			}			

		}
		return View::make('general.login');
	}

	public function image($image, $width = 0, $height = 0, $type = "")
    {
        $folderImagePath = CommonHelper::normalizePath(Config::get('webservice.epub_folder'));
        if($width > 0 && $height > 0)
        {
            $filePath = CommonHelper::resizeImage($image, $width, $height, $type);
        }
        else
        {
            $filePath = $image;
        }
        return Redirect::to(asset('assets/upload/'.$filePath));
    }
}
