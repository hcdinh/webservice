<?php

class UserController extends AccountController
{
    public function __construct()
    {
        $this->beforeFilter(function()
        {            
            if(!CommonHelper::isAdmin())
            {                
                return Redirect::to('category');                
            }

        });
    }


    public function index()
    {
        $data = array();
        $data['users'] = UserModel::orderBy('create_date', 'desc')->paginate(15);
        return View::make('user.list', $data);
    }

    public function search()
    {
        if (Request::isMethod('post'))
        {
            $textSearch = Input::get('text_search');
            $data['isSearch'] = "Showing results for '".$textSearch."'";
            $data['users'] = UserModel::where('user_name', 'like', "%{$textSearch}%")->orWhere('email', 'like', "%{$textSearch}%")->paginate(15);
            return View::make('user.list', $data);
        }
        return Redirect::to('user');
    }

    public function detail($id = 0)
    {
        $user = UserModel::find($id);
        if(isset($user))
        {
            $data['user'] = $user->toArray();
            $data['devices'] = $user->devices()->get();
            $data['payments'] = $user->payments()->orderBy('create_date','desc')->paginate(10);
        }
        else
        {
            $data['noUser'] = true;
        }
        return View::make('user.detail', $data);
    }

	public function edit($id = 0)
	{
        $data = array('type' => 'edit');
        $data['header'] = 'Edit user';
        $data['action'] = URL::to('user/edit/'.$id);
        if (Request::isMethod('post'))
        {
            $user = Input::get('data');
            if(empty($user['password']))
            {
                unset($user['password']);
                $dataValidate = $user;
            }
            else
            {
                $dataValidate = array_merge($user, array('confirm_password' => Input::get('confirm_password')));
            }

            $validator = Validator::make($dataValidate, $this->getValidationRuleUser('edit'));
            if ($validator->fails())
            {
                $messages = $validator->messages();
                $data['formErrors'] = $messages;
                $data['user'] = $user;
                return View::make('user.edit', $data);
            }

            $user['bored_date'] = date('Y-m-d h:i:s',strtotime($user['bored_date']));
            if(!empty($user['password']))
            {
                $user['password'] = md5($user['password']);
            }
            UserModel::where('id_user', $id)->update($user);
            return Redirect::to('user');
        }
        else
        {
            $user = UserModel::find($id);
            if(isset($user))
            {
                $data['user'] = $user->toArray();
            }
            else
            {
                $data['noUser'] = true;
            }
            return View::make('user.edit', $data);
        }
	}

    public function create()
    {
        $data = array('type' => 'new');
        $data['header'] = 'Create user';
        $data['action'] = URL::to('user/create');
        if (Request::isMethod('post'))
        {
            $user = Input::get('data');
            $dataValidate = array_merge($user, array('confirm_password' => Input::get('confirm_password')));

            $validator = Validator::make($dataValidate, $this->getValidationRuleUser('create'));
            if ($validator->fails())
            {
                $messages = $validator->messages();
                $data['formErrors'] = $messages;
                $data['user'] = $user;
                return View::make('user.edit', $data);
            }
            $user['create_date'] = date('Y-m-d h:i:s');
            $user['bored_date'] = date('Y-m-d h:i:s',strtotime($user['bored_date']));
            $user['password'] = md5($user['password']);
            UserModel::insert($user);
            return Redirect::to('user');
        }
        else
        {
            return View::make('user.edit', $data);
        }
    }

    public function delete($id)
    {
    	$user = UserModel::find($id);
    	$data = array();
        if(isset($user))
        {
            $data['user'] = $user;
        }
        else
        {
            return Redirect::to('user');
        }
    	return View::make('user.delete', $data);
    }

    public function doDelete($id)
    {
    	$user = UserModel::find($id);
        if(isset($user))
        {
            $user->devices()->delete();
            $user->payments()->delete();
    		$user->delete();
        }
        return Redirect::to('user');
    }

    private function getValidationRuleUser($type)
    {
        if($type == 'create')
        {
            return  array(
                'email' => 'required|email|unique:user,email',
                'user_name' => 'required',
                'password' => 'required|min:6|same:confirm_password',
                'confirm_password' => 'required|min:6',
                'bored_date' => 'required|date|date_format:d-m-Y',
                'sex' => 'required',
            );
        }

        if($type == 'edit')
        {
            return  array(
                'email' => 'required|email',
                'user_name' => 'required',
                'password' => 'max:255|min:6|same:confirm_password',
                'confirm_password' => 'max:255|required_with:password|min:6',
                'bored_date' => 'required|date|date_format:d-m-Y',
                'sex' => 'required',
            );
        }

    }


    public function delDevice($id, $userId)
    {
        $device = DeviceModel::find($id);
        if(isset($device))
        {
            $device->delete();
        }
        $user = UserModel::find($userId);
        if(isset($user))
        {
            $user->numDevice--;
            $user->save();
        }
        return Redirect::to('user/detail/'.$userId);

    }

}
