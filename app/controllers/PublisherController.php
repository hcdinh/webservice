<?php

class PublisherController extends AccountController 
{
    public function __construct()
    {
        $this->beforeFilter(function()
        {
            $crAction = CommonHelper::getCurrentAction();
            $arrayOnlyAdmin = array('edit', 'create', 'delete', 'doDelete');
            if(!CommonHelper::isAdmin())
            {
                if(in_array($crAction, $arrayOnlyAdmin))
                {
                    return Redirect::to('publisher');
                }
            }
        });
    }

    public function index()
    {
        $data = array();
        $data['publishers'] = PublisherModel::paginate(15);
        return View::make('publisher.list', $data);

    }

    public function search()
    {
        if (Request::isMethod('post'))
        {
            $textSearch = Input::get('text_search');
            $data['isSearch'] = "Showing results for '".$textSearch."'";            
            $data['publishers'] = PublisherModel::where('publisher_name', 'like', "%{$textSearch}%")->paginate(15);
            return View::make('publisher.list', $data);
        }        
        return Redirect::to('publisher');
    }

	public function edit($id = 0)
	{
        $data = array('type' => 'edit');
        $data['header'] = 'Edit publisher';
        $data['action'] = URL::to('publisher/edit/'.$id);
        if (Request::isMethod('post'))
        {
            $publisher = Input::get('data');
            $validator = Validator::make($publisher, $this->getValidationRule());
            if ($validator->fails())
            {
                $messages = $validator->messages();
                $data['formErrors'] = $messages;
                $data['publisher'] = $publisher;
                return View::make('publisher.edit', $data);
            }

            PublisherModel::where('id_publisher', $id)->update($publisher);
            return Redirect::to('publisher');
        }
        else
        {
            $publisher = PublisherModel::find($id);
            if(isset($publisher))
            {
                $data['publisher'] = $publisher->toArray();
            }
            else
            {
                $data['noPublisher'] = true;
            }
            return View::make('publisher.edit', $data);
        }  
	}

    public function create()
    {
        $data = array('type' => 'new');
        $data['header'] = 'Create publisher';
        $data['action'] = URL::to('publisher/create');
        if (Request::isMethod('post'))
        {
            $publisher = Input::get('data');            
            $validator = Validator::make($publisher, $this->getValidationRule());
            if ($validator->fails())
            {
                $messages = $validator->messages();
                $data['formErrors'] = $messages;
                $data['publisher'] = $publisher;
                return View::make('publisher.edit', $data);
            }            
            $publisher['create_date'] = date('Y-m-d h:i:s');
            PublisherModel::insert($publisher);
            return Redirect::to('publisher');
        }
        else
        {
            return View::make('publisher.edit', $data);
        }
    }

    public function delete($id)
    {
    	$publisher = PublisherModel::find($id);
    	$data = array();
        if(isset($publisher))
        {
            $data['publisher'] = $publisher;
        }
        else
        {
            return Redirect::to('publisher');
        }
    	return View::make('publisher.delete', $data);
    }

    public function doDelete($id)
    {
    	$publisher = PublisherModel::find($id);    	
        if(isset($publisher))
        {
    		$publisher->delete();        
        }
        return Redirect::to('publisher');
    }

    private function getValidationRule()
    {
        return  array(
            'publisher_name' => 'required',            
        );    
    }

}
