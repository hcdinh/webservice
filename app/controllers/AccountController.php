<?php

class AccountController extends BaseController 
{
	public function __construct()
    {
        $this->beforeFilter(function()
        {
            if(is_null(Session::get('user', null)))
            {
            	$userId = Cookie::get('webservice');
            	if(!is_null($userId))
            	{
            		$userData = UserModel::find($userId)->toArray(); 
            		if(isset($userData) && count($userData) > 0)           		
            		{
            			Session::put('user', $userData);	
            		}
            		
            	}
            	else
            	{
            		return Redirect::to('login');	
            	}
            	
            }
        });
    }


	public function profile()
	{
		return View::make('account.profile');
	}

	public function logout()
	{
		Session::forget('user');
		$cookie = Cookie::forget('webservice');
		return Redirect::to('login')->withCookie($cookie);
	}

    public function changePassword()
    {
        if(Request::isMethod('post'))
        {
            $data = Input::all();
            $rule = array('password' => 'required|min:8|confirmed',
                            'password_confirmation' => 'required|min:8');
            $validator = Validator::make($data, $rule);   
            if ($validator->fails())
            {
                $messages = $validator->messages();
                $data['formErrors'] = $messages;                
                return View::make('account.profile', $data);
            }
            $currentUser = Session::get('user');            
            UserModel::where('id_user', $currentUser['id_user'])->update(array('password' => md5($data['password'])));            
            return Redirect::to('profile');
        }
        return Redirect::to('profile');
    }
}
