<?php

class SpecialController extends AccountController
{
    public function index()
    {
        $promotionModel = PromotionModel::initInstance();
        $productModel = ProductModel::initInstance();
        $data = array();
        if(Session::get('user')['permission'] == 1)
        {
            $data['promotions'] = $promotionModel->getListPromotion();    
            $data['normals'] = $productModel->getListNotPromotion();
        }
        else
        {
            $data['promotions'] = $promotionModel->getListPromotion(Session::get('user')['id_user']);    
            $data['normals'] = $productModel->getListNotPromotion(Session::get('user')['id_user']);   
        }        
        return View::make('special.list', $data);
    }

    public function change()
    {
        $action = Input::get('action');
        $listNormal = Input::get('normal');
        $listNormal = explode(",", $listNormal);
        $listPro = Input::get('pro');
        $listPro = explode(",", $listPro);
        if($action == "topro")
        {  
            if(count($listNormal) > 0)
            {
                $dataInsert = array();
                foreach ($listNormal as $productId) 
                {
                    $dataInsert[] = array('id_product' => $productId);
                }
                PromotionModel::insert($dataInsert);
            }
        }
        else
        {
            if(count($listPro) > 0)
            {
                PromotionModel::whereIn('id_product', $listPro)->delete();
            }
        }
        return Redirect::to('special');
    }
}
