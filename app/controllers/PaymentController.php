<?php

class PaymentController extends AccountController 
{
    public function index()
    {
        $data = array();
        if(Session::get('user')['permission'] == 1)
        {
            $data['payments'] = PaymentModel::orderBy('create_date', 'desc')->paginate(15);
        }
        else
        {            
            $paymentModel = PaymentModel::initInstance();
            $data['payments'] = $paymentModel->getListPayment(Session::get('user')['id_user']);
        }
        
        return View::make('payment.list', $data);

    }

    public function search()
    {
        if (Request::isMethod('post'))
        {
            $textSearch = Input::get('text_search');
            $data['isSearch'] = "Showing results for '".$textSearch."'";            
            $data['payments'] = PaymentModel::where('payment_name', 'like', "%{$textSearch}%")->orderBy('create_date', 'desc')->paginate(15);
            return View::make('payment.list', $data);
        }        
        return Redirect::to('payment');
    }    
}
