<?php
class DeviceModel extends Eloquent
{
	protected $table = 'user_device';

	protected $primaryKey = 'id_device';

	public static $_instance = null;

	public $timestamps = false;

	public static function initInstance()
	{
		if(is_null(self::$_instance))
		{
			self::$_instance = new DeviceModel();
		}
		return self::$_instance;
	}

	public function checkDeviceUser($userId, $numberDevice, $deviceName)
	{
		$dataDevice = self::where('id_user', $userId)->where('device_name', $deviceName)->get()->toArray();		
		if(count($dataDevice) > 0)
		{
			return TRUE;			
		}
		else
		{			
			if($numberDevice < 3)
			{
				$dataInsert = array();
				$dataInsert['id_user'] = $userId;
				$dataInsert['device_name'] = $deviceName;
				$dataInsert['create_date'] = date('Y-m-d h:i:s');
				self::insert($dataInsert);

				//Update number device
				$numberDevice++;
				UserModel::where('id_user', $userId)->update(array('numDevice'=>$numberDevice));
				return TRUE;
			}
		}
		return FALSE;
	}
}
