<?php
class PromotionModel extends Eloquent
{
	protected $table = 'promotion_product';

	protected $primaryKey = 'id_promotion_product';

	public static $_instance = null;

	public $timestamps = false;
	
	public static function initInstance()
	{
		if(is_null(self::$_instance))
		{
			self::$_instance = new PromotionModel();
		}
		return self::$_instance;
	}

	public function product()
    {
        return $this->hasOne('ProductModel', 'id_product', 'id_product');
    }

    public function getListPromotion($userId = 0)
    {
    	if($userId == 0)
    	{
    		return self::join('products', 'products.id_product', '=', 'promotion_product.id_product')->where('isEnable',1)->orderBy('subtitle')->get();
    	}
		return self::join('products', 'products.id_product', '=', 'promotion_product.id_product')->where('isEnable',1)->where('id_user', $userId)->orderBy('subtitle')->get();
    }    
}
