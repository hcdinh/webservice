<?php
class PaymentModel extends Eloquent
{
	protected $table = 'payment';

	protected $primaryKey = 'id_payment';

	public static $_instance = null;

	public $timestamps = false;

	public function user()
    {
        return $this->hasOne('UserModel', 'id_user', 'id_user');
    }

    public function product()
    {
        return $this->hasOne('ProductModel', 'id_product', 'id_product');
    }

	public static function initInstance()
	{
		if(is_null(self::$_instance))
		{
			self::$_instance = new PaymentModel();
		}
		return self::$_instance;
	}

	public function checkBuyProduct($userId,$productId)    
    {
        $data = self::where('id_user', $userId)->where('id_product', $productId)->get()->toArray();
        if(count($data) > 0)
        {
        	if($data[0]['isPromotion'] == 1)
        	{
        		$user = UserModel::find($userId);
        		if(strtotime($user->start_date) < time() && time() < strtotime($user->end_date))
        		{
        			return BUY_PROMOTION;
        		}
        		return BUY_PROMOTION_EXPIRED;
        	}
        	if($data[0]['isPromotion'] == 0)
        	{        		
        		return BUY_FOREVER;
        	}
        }
        return NOT_BUY;
    }

    public function getListPayment($userId = 0)
    {
        $arrListPayment = self::join('products', 'products.id_product', '=' ,'payment.id_product')
                        ->where('products.id_user', $userId)->lists('id_payment');
        return self::whereIn('id_payment', $arrListPayment)->orderBy('create_date', 'desc')->paginate(15);

    }
}
