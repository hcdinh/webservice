<?php
class CategoryModel extends Eloquent
{
	protected $table = 'category';

	protected $primaryKey = 'id_category';

	public static $_instance = null;

	public $timestamps = false;

	public static function initInstance()
	{
		if(is_null(self::$_instance))
		{
			self::$_instance = new CategoryModel();
		}
		return self::$_instance;
	}
}
