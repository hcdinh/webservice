<?php
class PublisherModel extends Eloquent
{
	protected $table = 'publisher';

	protected $primaryKey = 'id_publisher';

	public static $_instance = null;

	public $timestamps = false;

	public static function initInstance()
	{
		if(is_null(self::$_instance))
		{
			self::$_instance = new PublisherModel();
		}
		return self::$_instance;
	}
}
