<?php
class UserModel extends Eloquent
{
	protected $table = 'user';

	protected $primaryKey = 'id_user';

	public static $_instance = null;

	public $timestamps = false;

	public static function initInstance()
	{
		if(is_null(self::$_instance))
		{
			self::$_instance = new UserModel();
		}
		return self::$_instance;
	}

	public function payments()
	{
		return $this->hasMany('PaymentModel','id_user','id_user');
	}

	public function devices()
	{
		return $this->hasMany('DeviceModel','id_user','id_user');
	}

	/**
	 * This function check email and pass for login
	 * @param  string $email    Email
	 * @param  string $password password
	 * @return mixed
	 */
	public function checkUserLogin($email, $password)
	{
		$userData = self::select()->where('email',$email)
								->where('password', md5($password))
								->where('permission', "<>", 0)
								->where('isEnable', 1)
								->get()->toArray();
		if(count($userData) == 0)
		{
			return FALSE;
		}
		return $userData[0];
	}

	public function checkUserLoginAPI($email, $password)
	{
		$userData = self::select()->where('email',$email)
								->where('password', md5($password))
								->where('permission', 0)
								->where('isEnable', 1)
								->get()->toArray();
		if(count($userData) == 0)
		{
			return FALSE;
		}
		return $userData[0];
	}

	public function getUserByToken($token)
	{
		$userData = self::select()->where('idToken', $token)->get()->toArray();
		if(count($userData) > 0)
		{
			return $userData[0];
		}
		return FALSE;
	}

	/**
	 * This function check email exist in fbsql_database
	 * @param string $email Email
	 * @return bool
	 */
	public function checkEmail($email)
	{
		$rs = self::where('email', $email)->first();
		if(is_null($rs))
		{
			return FALSE;
		}
		return TRUE;
	}

	public function updatePassword($userId, $oldPass, $newPass)
	{
		$user = self::where();
	}
}
