<?php
class ProductModel extends Eloquent
{
	protected $table = 'products';

	protected $primaryKey = 'id_product';

	public static $_instance = null;

	public $timestamps = false;

	public function category()
    {
        return $this->hasOne('CategoryModel', 'id_category', 'id_category');
    }

    public function publisher()
    {
        return $this->hasOne('PublisherModel', 'id_publisher', 'id_publisher');
    }    

    public function user()
    {
        return $this->hasOne('UserModel', 'id_user', 'id_user');
    }

	public static function initInstance()
	{
		if(is_null(self::$_instance))
		{
			self::$_instance = new ProductModel();
		}
		return self::$_instance;
	}

	public function getListNotPromotion($userId = 0)
	{
		$listPromotion = PromotionModel::lists('id_product');
		if(count($listPromotion) > 0)
		{
			if($userId == 0)
			{
				return self::whereNotIn('id_product', $listPromotion)->where('isEnable',YES)->orderBy('subtitle')->get();
			}
			else
			{
				return self::whereNotIn('id_product', $listPromotion)->where('isEnable',YES)->where('id_user', $userId)->orderBy('subtitle')->get();
			}
		}
		else
		{
			if($userId == 0)
			{
				return self::where('isEnable',YES)->orderBy('subtitle')->get();
			}
			else
			{
				return self::where('id_user', $userId)->where('isEnable',YES)->orderBy('subtitle')->get();
			}
		}
	}	

	public function getListProduct1($filter, &$totalPage, $numitem, $page)
	{
		if(!isset($filter['type']))
			return array();		

		$query = self::select()->where('isEnable',1);		
		switch ($filter['type'])
		{
			case 0: //Get all product
				break;
			case 1: //Get by category
				$query->where('id_category', $filter['id_category']);
				break;
			case 2: //Get free epub
				$query->where('price', 0);
				break;
			case 3: //Get promotion ebup
				$query->join('promotion_product', 'products.id_product', '=', 'promotion_product.id_product');
				break;
			case 4: //Get bought product
				$query->join('payment', 'payment.id_product', '=', 'products.id_product')
						->where('payment.id_user', $filter['id_user']);			
				break;
		}

		$total = count($query->get()->toArray());		
		$totalPage = floor($total/$numitem);
		if($total%$numitem != 0)
		{
			$totalPage++;
		}

		$numskip = $numitem*$page;		
		$products = $query->orderBy('products.create_date','desc')->skip($numskip)->take($numitem)->get();

		//Prepare data
		$data = array();		
		foreach ($products as $product) 
		{
			$temp = array();
			$temp['id_category'] = $product->id_category;
			$temp['id_product'] = $product->id_product;
			$temp['subtitle'] = $product->subtitle;
			$temp['price'] = $product->price;
			$temp['numPayment'] = $product->numPayment;
			$temp['link_image'] = URL::to('image/'.$product->link_image);
			$data[] = $temp;
		}
		return $data;
	}

	public function getListProduct2($type, &$totalPage, $page)
	{
		if($type != 0 && $type != 1)
			return array();		

		$query = self::select()->where('isEnable',1);		
		switch ($type)
		{
			case 0: //Get best buy product
				$query->orderBy('numPayment', 'desc');
				break;
			case 1: //Get by category
				$query->orderBy('products.create_date','desc');
				break;			
		}
		$numskip = 10*$page;
		$numtake = 20 - $numskip;
		$products = $query->skip($numskip)->take($numtake)->get();
		$total = count($products);
		if($total > 10)
		{
			$totalPage = 2;
		}
		else
		{
			$totalPage = 1;
		}

		//Prepare data
		$data = array();
		foreach ($products as $product) 
		{
			$temp = array();
			$temp['id_category'] = $product->id_category;
			$temp['id_product'] = $product->id_product;
			$temp['subtitle'] = $product->subtitle;
			$temp['price'] = $product->price;
			$temp['numPayment'] = $product->numPayment;
			$temp['link_image'] = URL::to('image/'.$product->link_image);
			$data[] = $temp;
		}
		return $data;
	}
}

