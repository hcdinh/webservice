<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>
                @section('title')
                Webservice
                @show
        </title>

        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/vendor/bootstrap.min.css')}}">

        <!-- MetisMenu CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/vendor/metisMenu.min.css')}}">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/vendor/sb-admin-2.css')}}">

        <!-- Custom Fonts -->
        <link rel="stylesheet" href="{{asset('assets/css/font-awesome.min.css')}}">

        <!-- Datepicker -->
        <link rel="stylesheet" href="{{asset('assets/css/vendor/datepicker3.css')}}">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
	<body>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" id ="divNav" style="margin-bottom: 0;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Webservice</a>
                </div>
                <!-- /.navbar-header -->

                <ul class="nav navbar-top-links navbar-right">
                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-user fa-fw"></i> {{Session::get('user')['user_name']}} <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="{{URL::to('profile')}}"><i class="fa fa-user fa-fw"></i> Change password</a>
                            </li>
                            <li class="divider"></li>
                            <li><a href="{{URL::to('logout')}}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                            </li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- /.dropdown -->
                </ul>
                <!-- /.navbar-top-links -->

                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <ul class="nav" id="side-menu"> 
                            @if(Request::segment(1) != 'special')
                            <li class="sidebar-search">
                                <form method="POST" action="{{URL::to(Request::segment(1).'/search')}}">
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" name ="text_search" placeholder="Search {{Request::segment(1)}}...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="submit">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </span>
                                </div>
                                </form>
                                <!-- /input-group -->
                            </li>      
                            @endif                                                 
                            <li>
                                <a class="@if(Request::segment(1) == 'category') active @endif" href="{{URL::to('category')}}"><i class="fa fa-folder fa-fw"></i> Category</a>
                            </li>
                            <li>
                                <a class="@if(Request::segment(1) == 'publisher') active @endif" href="{{URL::to('publisher')}}"><i class="fa fa-building fa-fw"></i> Publisher</a>
                            </li>
                            <li>
                                <a class="@if(Request::segment(1) == 'product') active @endif" href="{{URL::to('product')}}"><i class="fa fa-book fa-fw"></i> Product</a>
                            </li>
                            <li>
                                <a class="@if(Request::segment(1) == 'payment') active @endif" href="{{URL::to('payment')}}"><i class="fa fa-money fa-fw"></i> Payment</a>
                            </li>                            
                            @if(Session::get('user')['permission'] == 1)
                            <li>
                                <a class="@if(Request::segment(1) == 'special') active @endif" href="{{URL::to('special')}}"><i class="fa fa-star fa-fw"></i> Special Product</a>
                            </li>                            
                            <li>
                                <a class="@if(Request::segment(1) == 'user') active @endif" href="{{URL::to('user')}}"><i class="fa fa-users fa-fw"></i> User</a>
                            </li>
                            @endif
                        </ul>
                    </div>
                    <!-- /.sidebar-collapse -->
                </div>
                <!-- /.navbar-static-side -->
            </nav>

            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            @section('header')
                            @show
                        </h1>
                    </div>
                    <div class="col-lg-12">
                        <!-- Content -->
                        @yield('content')
                        <!-- ./ content -->
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

            </div>
            <!-- /#page-wrapper -->

        </div>



		<!-- Javascripts
		================================================== -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script src="{{asset('assets/css/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/js/metisMenu.min.js')}}"></script>
        <script src="{{asset('assets/js/sb-admin-2.js')}}"></script>

        @yield('scripts')
	</body>
</html>
