<!DOCTYPE html>
<html>
<head>
	<title>Test API</title>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>
<body>
<form method="POST" action="" id="form">
	<span style ="width:100px; display:inline-block">API: </span>
	<select id="api">
		<option>--Select API--</option>
		<option value="signin">Sign in</option>
		<option value="login">Login</option>
		<option value="logout">Logout</option>
		<option value="update">Update password</option>
		<option value="check_login">Check login</option>
		<option value="get_product">Download Epub</option>
	</select>	
	<br/>
	<span style ="width:100px; display:inline-block">Test data: </span>
	<textarea id="data" name = "input" rows="20" cols="50"></textarea>
	<br/>
	<button id="btn">View result</button>		
</form>

<script type="text/javascript">
	$(document).ready(function () {
		$("#api").change(function(){
			var temp = $(this).val();			
			var jsArr = {};			
			switch(temp) {
			    case "signin":
			        jsArr.email = "testapi@gmail.com";			       
			        jsArr.password = "123456";	
			        jsArr.user_name = "hugo";	
			        jsArr.bored_date = "2014-10-12";	
			        jsArr.sex = 1;	
			        jsArr.device_name = "iphone";	
			        break;			   
			    case "login":
			        jsArr.email = "testapi@gmail.com";			       
			        jsArr.password = "123456";				        
			        jsArr.device_name = "iphone";	
			        break;
			    case "logout":
			        jsArr.token = "";			        
			        jsArr.device_name = "iphone";	
			        break;
			    case "update":
			        jsArr.token = "";			        
			        jsArr.device_name = "iphone";
			        jsArr.oldpass = "123456";
			       	jsArr.newpass = "1234567";
			        break;
			    case "check_login":
			    	jsArr.token = "";
			    	jsArr.device_name = "iphone";
			    	break;
			    case "get_product":
			    	jsArr.token = "";
			    	jsArr.device_name = "iphone";
			    	jsArr.kind_product = "1";
			    	jsArr.id_product = "2";
			    	break;
			}
			$("#data").val(JSON.stringify(jsArr, undefined, 2));
		});
		$("#btn").click(function (){
			var action = 'http://kais.meximas.com/webservice/api/v1/' + $("#api").val();
			$('#form').get(0).setAttribute('action', action);
			$('#form').submit(); 
		});
	});
</script>
</body>
</html>