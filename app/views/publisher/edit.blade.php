@extends('layouts.default')

{{-- Header --}}
@section('header')
{{$header}}
@stop

{{-- Content --}}
@section('content')
@if (!isset($noPublisher))
@if(isset($formErrors))
	<div class="alert alert-danger" role="alert">
		<ul>
			@foreach ($formErrors->all('<li>:message</li>') as $error)
		    {{$error}}
			@endforeach
		</ul>
	</div>
@endif
@if(isset($singleError))
	<div class="alert alert-danger" role="alert">
		{{$singleError}}
	</div>
@endif
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<form class="form-horizontal" role="form" method="POST" action ="{{$action}}">
			<input type="hidden" name="type" value="{{$type}}"/>
			<div class="form-group">
				<label for="inputPublisherName" class="col-sm-3 control-label">Publisher Name</label>
				<div class="col-sm-9">
				  <input class="form-control" id="inputPublisherName" name="data[publisher_name]" placeholder="Publisher name" value="@if(isset($publisher['publisher_name'])){{$publisher['publisher_name']}}@endif">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Address</label>
				<div class="col-sm-9">
					<textarea name="data[address_publisher]"  class="form-control" rows="3">@if(isset($publisher['address_publisher'])){{$publisher['address_publisher']}}@endif</textarea>
				</div>
			</div>
			<hr/>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
				@if($type == 'edit')
					<button type="submit" class="btn btn-primary">Update</button>
					<a href="{{URL::to('publisher')}}" class="btn btn-default">Cancel</a>
				@else
					<button type="submit" class="btn btn-primary">Create</button>
					<a href="{{URL::to('publisher')}}" class="btn btn-default">Return to list</a>
				@endif
				</div>
			</div>
		</form>
	</div>
</div>
@else
<div class="alert alert-danger" role="alert">Publisher isn't existed!</div>
@endif
@stop

