@extends('layouts.default')

{{-- Header --}}
@section('header')
@if(isset($isSearch))
{{$isSearch}}
@else
Publisher Manager
<div class="pull-right">
@if(CommonHelper::isAdmin())
<a href="{{URL::to('publisher/create')}}" class="btn btn-success">Create</a>
@endif
</div>
@endif
@stop

{{-- Content --}}
@section('content')
	<table class="table">      
      <thead>
        <tr>
          <th>Publisher Name</th>
          <th>Address</th>
          <th>Create Date</th>    
          @if(CommonHelper::isAdmin())     
          <th></th>
          @endif
        </tr>
      </thead>
      <tbody>
      	@foreach($publishers as $publisher)
        <tr>
          <td>{{$publisher->publisher_name}}</td>
          <td>{{$publisher->address_publisher}}</td>       
          <td>{{date('d-m-Y', strtotime($publisher->create_date))}}</td>
          @if(CommonHelper::isAdmin())
          <td>
          	<a href="{{URL::to('publisher/edit/'.$publisher->id_publisher)}}" class="btn btn-primary">Edit</a>
          	<a href="{{URL::to('publisher/delete/'.$publisher->id_publisher)}}" class="btn btn-danger">Delete</a>
          </td>
          @endif
        </tr>        
        @endforeach
      </tbody>
    </table>
    {{$publishers->links()}}

@stop
