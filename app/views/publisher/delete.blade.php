@extends('layouts.default')

{{-- Header --}}
@section('header')
Delete publisher
@stop

{{-- Content --}}
@section('content')
<div class="panel panel-danger">
    <div class="panel-heading">
        Delete confirmation
    </div>
    <div class="panel-body">      
        <p>Publisher: {{$publisher->publisher_name}}</p>
        <p>Address: {{$publisher->address_publisher}}</p>
        <hr/>
        <p>Are you sure delete this publisher? </p>
      
    </div>
    <div class="panel-footer">
      <div class="pull-right">
          <a href="{{URL::to('publisher')}}" class="btn btn-default">Cancel</a>
          <a href="{{URL::to('publisher/doDelete/'.$publisher->id_publisher)}}" class="btn btn-primary">Delete</a>
      </div>
      <div class="clearfix">
      </div>
    </div>
</div>


@stop
