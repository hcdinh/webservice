@extends('layouts.default')

{{-- Header --}}
@section('header')
Special Product
@stop

{{-- Content --}}
@section('content')
<form action="{{URL::to('special/change')}}" method="post" id="form">
<input type="hidden" name="normal" id="ip_normal">
<input type="hidden" name="pro" id="ip_pro">
<input type="hidden" name="action" id="ip_action">
<div class="row">
  <div class="col-md-5">
    
    <div class="panel panel-success" style="height:450px; overflow:auto">
      <div class="panel-heading">Normal</div>
      <div class="panel-body">
        @foreach($normals as $normal)
          <div class="well well-sm" id="normal_{{$normal->id_product}}">{{$normal->subtitle}}</div>
        @endforeach  
      </div>
    </div>
  </div>    
  
  <div class="col-md-2" align="center" style="display:table-cell">
    <button type="button" class="btn btn-default" id="bt_right"><i class="fa fa-long-arrow-right"></i></button>
    <br/>
    <br/>
    <button type="button" class="btn btn-default" id="bt_left"><i class="fa fa-long-arrow-left"></i></button>
    <br/>
    <br/>
  </div>
  <div class="col-md-5">    
    <div class="panel panel-info" style="height:450px; overflow:auto">
      <div class="panel-heading">Promotion</div>
      <div class="panel-body">
        @foreach($promotions as $promotion)
          <div class="well well-sm" id="pro_{{$promotion->id_product}}">{{$promotion->subtitle}}</div>
        @endforeach  
      </div>
    </div>
  </div>   
</div>
</form>
@stop

{{-- Javascript --}}
@section('scripts')
<script src="{{asset('assets/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript">
  var listNormal = [];
  var listPro = [];
  $(document).ready(function (){
    $("#bt_right").click(function () {
      $("#ip_normal").val(listNormal.toString());
      $("#ip_pro").val(listPro.toString());
      $("#ip_action").val("topro");
      $("#form").submit();
    });
    $("#bt_left").click(function () {
      $("#ip_normal").val(listNormal.toString());
      $("#ip_pro").val(listPro.toString());
      $("#ip_action").val("tonormal");
      $("#form").submit();
    });

    $(".well").click(function () {
      data = $(this).attr('id');
      data = data.split("_");
      if(data[0] == 'normal')
      {
        index = $.inArray(data[1],listNormal)
        if(index == -1)
        {
          listNormal.push(data[1]);
          $(this).attr('style', 'background-color:#D9EDF7');
        }
        else
        {
           listNormal.splice(index, 1);
           $(this).removeAttr('style');
        }

      }
      else
      {
        index = $.inArray(data[1],listPro)
        if(index == -1)
        {
          listPro.push(data[1]);
          $(this).attr('style', 'background-color:#D9EDF7');
        }
        else
        {
           listPro.splice(index, 1);
           $(this).removeAttr('style');
        }
      }
      
    });
  });

</script>
@stop

