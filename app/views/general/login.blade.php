@extends('layouts.home')

{{-- Web site Title --}}
@section('title')
Login ::
@parent
@stop

{{-- Content --}}
@section('content')
        <div class="lg-wapper">        
            <div class="login-wp">
                <div class="sitename">
                    <div>Webservice</div>
                </div>
                <form method="POST" action="{{{  URL::to('/login') }}}" accept-charset="UTF-8">
                    <div class="login-form">
                        <input type="hidden" name="_token" value="{{{ Session::getToken() }}}">                        
                        <input tabindex="1" placeholder="Email" type="text" name="email" id="email" value=""><br>
                        <input tabindex="2" placeholder="Password" type="password" name="password" id="password">
                        <label for="remember" class="checkbox lg-label">Remember me
                            <input type="hidden" name="remember" value="0">
                            <input tabindex="4" type="checkbox" name="remember" id="remember" value="1">
                        </label>
                        <button tabindex="3" type="submit">Login</button>                        
                        @if ( Session::get('error') )
                            <div class="lg-error">{{{ Session::get('error') }}}</div>
                        @endif
                        @if ( Session::get('notice') )
                            <div class="lg-error">{{{ Session::get('notice') }}}</div>
                        @endif
                    </div>
                </form>                            
            </div>
        </div>
        
        <!-- ./ container -->  
@stop
