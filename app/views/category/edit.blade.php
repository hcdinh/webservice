@extends('layouts.default')

{{-- Header --}}
@section('header')
{{$header}}
@stop

{{-- Content --}}
@section('content')
@if (!isset($noCategory))
@if(isset($formErrors))
	<div class="alert alert-danger" role="alert">
		<ul>
			@foreach ($formErrors->all('<li>:message</li>') as $error)
		    {{$error}}
			@endforeach
		</ul>
	</div>
@endif
@if(isset($singleError))
	<div class="alert alert-danger" role="alert">
		{{$singleError}}
	</div>
@endif
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<form class="form-horizontal" role="form" method="POST" action ="{{$action}}">
			<input type="hidden" name="type" value="{{$type}}"/>
			<div class="form-group">
				<label for="inputCategoryName" class="col-sm-3 control-label">Category Name</label>
				<div class="col-sm-9">
				  <input class="form-control" id="inputCategoryName" name="data[category_name]" placeholder="Category name" value="@if(isset($category['category_name'])){{$category['category_name']}}@endif">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>
				<div class="col-sm-9">
					<select class="form-control" name="data[isEnable]">
						<option value="1" @if(isset($category['isEnable']) && $category['isEnable'] == 1) selected @endif>Enable</option>
					  	<option value="0" @if(isset($category['isEnable']) && $category['isEnable'] == 0) selected @endif>Disable</option>					  
					</select>
				</div>
			</div>
			<hr/>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
				@if($type == 'edit')
					<button type="submit" class="btn btn-primary">Update</button>
					<a href="{{URL::to('category')}}" class="btn btn-default">Cancel</a>
				@else
					<button type="submit" class="btn btn-primary">Create</button>
					<a href="{{URL::to('category')}}" class="btn btn-default">Return to list</a>
				@endif
				</div>
			</div>
		</form>
	</div>
</div>
@else
<div class="alert alert-danger" role="alert">Category isn't existed!</div>
@endif
@stop

