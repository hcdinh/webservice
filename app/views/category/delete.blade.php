@extends('layouts.default')

{{-- Header --}}
@section('header')
Delete category
@stop

{{-- Content --}}
@section('content')
<div class="panel panel-danger">
    <div class="panel-heading">
        Delete confirmation
    </div>
    <div class="panel-body">      
        <p>Category: {{$category->category_name}}</p>
        <hr/>
        <p>Are you sure delete this category? </p>
      
    </div>
    <div class="panel-footer">
      <div class="pull-right">
          <a href="{{URL::to('category')}}" class="btn btn-default">Cancel</a>
          <a href="{{URL::to('category/doDelete/'.$category->id_category)}}" class="btn btn-primary">Delete</a>
      </div>
      <div class="clearfix">
      </div>
    </div>
</div>


@stop
