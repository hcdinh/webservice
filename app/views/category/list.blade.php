@extends('layouts.default')

{{-- Header --}}
@section('header')
@if(isset($isSearch))
{{$isSearch}}
@else
Category Manager
<div class="pull-right">
@if(CommonHelper::isAdmin())
<a href="{{URL::to('category/create')}}" class="btn btn-success">Create</a>
@endif
</div>
@endif
@stop

{{-- Content --}}
@section('content')
	<table class="table">      
      <thead>
        <tr>
          <th>Category Name</th>
          <th>Status</th>
          @if(CommonHelper::isAdmin())
          <th></th>          
          @endif
        </tr>
      </thead>
      <tbody>
      	@foreach($categories as $category)
        <tr>
          <td>{{$category->category_name}}</td>
          <td>
            @if($category->isEnable == 1) Yes @else No @endif
          </td>       
          @if(CommonHelper::isAdmin())
          <td>
          	<a href="{{URL::to('category/edit/'.$category->id_category)}}" class="btn btn-primary">Edit</a>
          	<a href="{{URL::to('category/delete/'.$category->id_category)}}" class="btn btn-danger">Delete</a>
          </td>
          @endif
        </tr>        
        @endforeach
      </tbody>
    </table>
    {{$categories->links()}}

@stop
