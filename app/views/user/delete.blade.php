@extends('layouts.default')

{{-- Header --}}
@section('header')
Delete user
@stop

{{-- Content --}}
@section('content')
<div class="panel panel-danger">
    <div class="panel-heading">
        Delete confirmation
    </div>
    <div class="panel-body">      
        <p>Username: {{$user->user_name}}</p>
        <p>Email: {{$user->email}}</p>
        <hr/>
        <p>Are you sure delete this user? </p>
      
    </div>
    <div class="panel-footer">
      <div class="pull-right">
          <a href="{{URL::to('user')}}" class="btn btn-default">Cancel</a>
          <a href="{{URL::to('user/doDelete/'.$user->id_user)}}" class="btn btn-primary">Delete</a>
      </div>
      <div class="clearfix">
      </div>
    </div>
</div>


@stop


{{-- Javascript --}}
@section('scripts')
<script src="{{asset('assets/js/bootstrap-datepicker.js')}}"></script>
@stop
