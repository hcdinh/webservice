@extends('layouts.default')

{{-- Header --}}
@section('header')
{{$header}}
@stop

{{-- Content --}}
@section('content')
@if (!isset($noUser))
@if(isset($formErrors))
	<div class="alert alert-danger" role="alert">
		<ul>
			@foreach ($formErrors->all('<li>:message</li>') as $error)
		    {{$error}}
			@endforeach
		</ul>
	</div>
@endif
@if(isset($singleError))
	<div class="alert alert-danger" role="alert">
		{{$singleError}}
	</div>
@endif
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<form class="form-horizontal" role="form" method="POST" action ="{{$action}}">
			<input type="hidden" name="type" value="{{$type}}"/>
			<div class="form-group">
				<label for="inputEmail" class="col-sm-3 control-label">Email</label>
				<div class="col-sm-9">
				  <input type="email" class="form-control" id="inputEmail" name="data[email]" placeholder="Email" value="@if(isset($user['email'])){{$user['email']}}@endif">
				</div>
			</div>

			<div class="form-group">
				<label for="inputUsername" class="col-sm-3 control-label">Username</label>
				<div class="col-sm-9">
				  <input class="form-control" id="inputUsername" name="data[user_name]" placeholder="Username" value="@if(isset($user['user_name'])){{$user['user_name']}}@endif">
				</div>
			</div>

			<div class="form-group">
				<label for="inputPW" class="col-sm-3 control-label">Password</label>
				<div class="col-sm-9">
				  <input class="form-control" type="password" id="inputPW" name="data[password]" placeholder="password" value="">
				</div>
			</div>

			<div class="form-group">
				<label for="inputConfirmPW" class="col-sm-3 control-label">Confirm password</label>
				<div class="col-sm-9">
				  <input class="form-control" type="password" id="inputConfirmPW" name="confirm_password" placeholder="Confirm password" value="">
				</div>
			</div>

			<div class="form-group">
				<label for="inputBirthday" class="col-sm-3 control-label">Birthday</label>
				<div class="col-sm-9">
				  <input class="form-control mydatepicker" id="inputBirthday" name="data[bored_date]" placeholder="Birthday" value="@if(isset($user['bored_date'])){{date('d-m-Y', strtotime($user['bored_date']))}}@endif">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Gender</label>
				<div class="col-sm-9">
				  	<label class="radio-inline">
						<input type="radio" name="data[sex]" id="inlineRadio1" value="0" @if(isset($user['sex']) && $user['sex'] == 0) checked @endif> Male
					</label>
					<label class="radio-inline">
						<input type="radio" name="data[sex]" id="inlineRadio2" value="1" @if(isset($user['sex']) && $user['sex'] == 1) checked @endif> Female
					</label>
				</div>
			</div>

			<div class="form-group">
				<label for="inputDeposit" class="col-sm-3 control-label">Deposit</label>
				<div class="col-sm-9">
				  <input class="form-control" id="inputDeposit" name="data[deposit]" placeholder="Deposit" value="@if(isset($user['deposit'])){{$user['deposit']}}@endif">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Permission</label>
				<div class="col-sm-9">
					<select class="form-control" name="data[permission]">
					  <option value="0" @if(isset($user['permission']) && $user['permission'] == 0) selected @endif>Normal User</option>
					  <option value="1" @if(isset($user['permission']) && $user['permission'] == 1) selected @endif>Admin</option>
					  <option value="2" @if(isset($user['permission']) && $user['permission'] == 2) selected @endif>Uploader</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>
				<div class="col-sm-9">
					<select class="form-control" name="data[isEnable]">
					  <option value="0" @if(isset($user['isEnable']) && $user['isEnable'] == 0) selected @endif>Disable</option>
					  <option value="1" @if(isset($user['isEnable']) && $user['isEnable'] == 1) selected @endif>Enable</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Promotion</label>
				<div class="col-sm-9">
					<label class="checkbox-inline">
						<input type="hidden" name="data[havePromotionUse]" value="0">
						<input type="checkbox" name="data[havePromotionUse]" value="1" @if(isset($user['havePromotionUse']) && $user['havePromotionUse'] == 1) checked @endif> Yes
					</label>
				</div>
			</div>
			<hr/>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
				@if($type == 'edit')
					<button type="submit" class="btn btn-primary">Update</button>
					<a href="{{URL::to('user')}}" class="btn btn-default">Cancel</a>
				@else
					<button type="submit" class="btn btn-primary">Create</button>
					<a href="{{URL::to('user')}}" class="btn btn-default">Return to list</a>
				@endif
				</div>
			</div>
		</form>
	</div>
</div>
@else
<div class="alert alert-danger" role="alert">User isn't existed!</div>
@endif
@stop


{{-- Javascript --}}
@section('scripts')
<script src="{{asset('assets/js/bootstrap-datepicker.js')}}"></script>
@stop
