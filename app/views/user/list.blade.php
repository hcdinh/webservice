@extends('layouts.default')

{{-- Header --}}
@section('header')
@if(isset($isSearch))
{{$isSearch}}
@else
User manager
<div class="pull-right">
<a href="{{URL::to('user/create')}}" class="btn btn-success">Create</a>
</div>
@endif
@stop

{{-- Content --}}
@section('content')
	<table class="table">      
      <thead>
        <tr>
          <th>Username</th>
          <th>Email</th>
          <th>Birthday</th>
          <th></th>          
        </tr>
      </thead>
      <tbody>
      	@foreach($users as $user)
        <tr>
          <td>{{$user->user_name}}</td>
          <td>{{$user->email}}</td>
          <td>{{date('d-m-Y', strtotime($user->bored_date))}}</td>          
          <td>
          	<a href="{{URL::to('user/edit/'.$user->id_user)}}" class="btn btn-primary">Edit</a>
          	<a href="{{URL::to('user/delete/'.$user->id_user)}}" class="btn btn-danger">Delete</a>
            <a href="{{URL::to('user/detail/'.$user->id_user)}}" class="btn btn-default">Detail</a>
          </td>
        </tr>        
        @endforeach
      </tbody>
    </table>
    {{$users->links()}}

@stop


{{-- Javascript --}}
@section('scripts')
<script src="{{asset('assets/js/bootstrap-datepicker.js')}}"></script>
@stop
