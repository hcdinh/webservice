@extends('layouts.default')

{{-- Header --}}
@section('header')
Detail
@stop

{{-- Content --}}
@section('content')
@if (!isset($noUser))
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-default">
			<div class="panel-heading"><h3 class="panel-title">Basic information</h3></div>
			<div class="panel-body">
				<div class="form-horizontal">
					<div class="form-group">
						<label for="inputEmail" class="col-sm-5 ">User ID</label>
						<div class="col-sm-7">
						  @if(isset($user['id_user'])){{$user['id_user']}}@endif
						</div>
					</div>

					<div class="form-group">
						<label for="inputEmail" class="col-sm-5 ">Email</label>
						<div class="col-sm-7">
						  @if(isset($user['email'])){{$user['email']}}@endif
						</div>
					</div>

					<div class="form-group">
						<label for="inputUsername" class="col-sm-5 ">Username</label>
						<div class="col-sm-7">
						  @if(isset($user['user_name'])){{$user['user_name']}}@endif
						</div>
					</div>

					<div class="form-group">
						<label for="inputBirthday" class="col-sm-5 ">Birthday</label>
						<div class="col-sm-7">
						  @if(isset($user['bored_date'])){{date('d-m-Y', strtotime($user['bored_date']))}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5 ">Gender</label>
						<div class="col-sm-7">
								@if(isset($user['sex']) && $user['sex'] == 0) Male @endif
								@if(isset($user['sex']) && $user['sex'] == 1) Female @endif
						</div>
					</div>

					<div class="form-group">
						<label for="inputDeposit" class="col-sm-5 ">Deposit</label>
						<div class="col-sm-7">
						  @if(isset($user['deposit'])){{$user['deposit']}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5 ">Permission</label>
						<div class="col-sm-7">
							  @if(isset($user['permission']) && $user['permission'] == 0) Normal User @endif
							  @if(isset($user['permission']) && $user['permission'] == 1) Admin @endif
							  @if(isset($user['permission']) && $user['permission'] == 2) Uploader @endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5 ">Status</label>
						<div class="col-sm-7">
							@if(isset($user['isEnable']) && $user['isEnable'] == 0) Disable @endif
							@if(isset($user['isEnable']) && $user['isEnable'] == 1) Enable @endif
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5 ">Promotion</label>
						<div class="col-sm-7">
								@if(isset($user['havePromotionUse']) && $user['havePromotionUse'] == 1) Yes @endif
								@if(isset($user['havePromotionUse']) && $user['havePromotionUse'] == 0) No @endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5 ">Create date</label>
						<div class="col-sm-7">
						  @if(isset($user['create_date'])){{date('d-m-Y', strtotime($user['create_date']))}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5 ">Number of Device</label>
						<div class="col-sm-7">
						  @if(isset($user['numDevice'])){{$user['numDevice']}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5 ">Session ID</label>
						<div class="col-sm-7">
						  @if(isset($user['idSession'])){{$user['idSession']}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5 ">Token ID</label>
						<div class="col-sm-7">
						  @if(isset($user['idToken'])){{$user['idToken']}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5 ">Lastest login date</label>
						<div class="col-sm-7">
						  @if(isset($user['lastest_login_date'])){{date('d-m-Y', strtotime($user['lastest_login_date']))}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5 ">Promotion Start Date</label>
						<div class="col-sm-7">
						  @if(isset($user['start_date'])){{date('d-m-Y', strtotime($user['start_date']))}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5 ">Promotion End Date</label>
						<div class="col-sm-7">
						  @if(isset($user['end_date'])){{date('d-m-Y', strtotime($user['end_date']))}}@endif
						</div>
					</div>

					<hr/>
					<div class="form-group">
						<div class="col-sm-offset-5 col-sm-7">
							<a href="{{URL::to('user/edit/'.$user['id_user'])}}" class="btn btn-primary">Edit</a>
							<a href="{{URL::to('user/delete/'.$user['id_user'])}}" class="btn btn-danger">Delete</a>
							<a href="{{URL::to('user')}}" class="btn btn-default">Return to list</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-success">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">User device</h3>
		  	</div>
		  	<div class="panel-body">
		  		<table class="table table-bordered">
					<thead>
				        <tr>
							<th>Device name</th>
							<th>Created at</th>
							<th>Status</th>
							<th></th>
				        </tr>
				    </thead>
				    <tbody>
				    	@foreach($devices as $device)
				        <tr>
				          <td>@if(isset($device->device_name)){{$device->device_name}}@endif</td>
				          <td>@if(isset($device->create_date)){{date('d-m-Y', strtotime($device->create_date))}}@endif</td>
				          <td>
				          	@if(isset($device->isEnable) && $device->isEnable == 0) Disable @endif
							@if(isset($device->isEnable) && $device->isEnable == 1) Enable @endif
				          </td>
				          <td>
				          	<button data-toggle="modal" data-target="#modelDeleleDevice" class="btn btn-xs btn-danger btn_del_device" id="device_{{$device->id_device}}">Remove</button>
				          </td>
				        </tr>
				        @endforeach
				    </tbody>
				</table>
		  	</div>
		</div>
		<div class="panel panel-primary">
		  	<div class="panel-heading">
		    	<h3 class="panel-title">User payment</h3>
		  	</div>
		  	<div class="panel-body">
		  		<table class="table table-bordered">
					<thead>
				        <tr>
							<th>Product</th>
							<th>Price</th>
							<th>Create Date</th>
							<th>Promotion</th>
							<th>Download</th>
				        </tr>
				    </thead>
				    <tbody>
				    	@foreach($payments as $payment)
				        <tr>
				          <td>@if(isset($payment->product->subtitle)){{$payment->product->subtitle}}@endif</td>
				          <td>@if(isset($payment->price)){{$payment->price}}@endif</td>
				          <td>@if(isset($payment->create_date)){{date('d-m-Y', strtotime($payment->create_date))}}@endif</td>
				          <td>
				          	@if(isset($payment->isPromotion) && $payment->isPromotion == 0) No @endif
							@if(isset($payment->isPromotion) && $payment->isPromotion == 1) Yes @endif
				          </td>
				          <td>
				          	@if(isset($payment->canDownload) && $payment->canDownload == 0) No @endif
							@if(isset($payment->canDownload) && $payment->canDownload == 1) Yes @endif
				          </td>
				        </tr>
				        @endforeach
				    </tbody>
				</table>
				{{$payments->links()}}
		  	</div>
		</div>
	</div>
</div>
@else
<div class="alert alert-danger" role="alert">User isn't existed!</div>
@endif


<!-- Modal -->
<div class="modal fade" id="modelDeleleDevice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Delete Confirmation</h4>
      </div>
      <div class="modal-body">
        Are you sure delete this device?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a id="confirmDelBtn" class="btn btn-danger">Delete</a>
      </div>
    </div>
  </div>
</div>


@stop


{{-- Javascript --}}
@section('scripts')
<script src="{{asset('assets/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript">

	$(".btn_del_device").click(function() {
		var device_id = $(this).attr('id').split("_");
		device_id = device_id[1];
		$('#confirmDelBtn').attr('href', "{{URL::to('user/deletedevice/')}}/"+device_id+"/{{$user['id_user']}}");
		$("#divNav").css('z-index', -1);
	});

	$('#modelDeleleDevice').on('hidden.bs.modal', function (e) {
		$("#divNav").css('z-index', 1);
	})
</script>
@stop
