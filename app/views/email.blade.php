
<h3>Thông tin mua sản phẩm</h3>
<div>
<span style="padding:10px">ID product: </span>
<span style="padding:10px">{{$product->id_product}}</span>
</div>
<div>
<span style="padding:10px">Product name: </span>
<span style="padding:10px">{{$product->subtitle}}</span>
</div>
<div>
<span style="padding:10px">Khuyến mãi: </span>
<span style="padding:10px">@if($payment['isPromotion'] == 0) NO @else YES @endif</span>
</div>
<div>
<span style="padding:10px">Giá: </span>
<span style="padding:10px">{{$payment['price']}}</span>
</div>
<div>
<span style="padding:10px">Ngày mua: </span>
<span style="padding:10px">{{date('d-m-Y')}}</span>
</div>
<div>
<span style="padding:10px">Email người mua: </span>
<span style="padding:10px">{{$user['email']}}</span>
</div>
<div>Xin vui lòng vào vào web server kiểm tra lại</div>