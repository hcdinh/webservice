@extends('layouts.default')

{{-- Header --}}
@section('header')
Change password
@stop

{{-- Content --}}
@section('content')
@if(isset($formErrors))
	<div class="alert alert-danger" role="alert">
		<ul>
			@foreach ($formErrors->all('<li>:message</li>') as $error)
		    {{$error}}
			@endforeach
		</ul>
	</div>
@endif
@if(isset($singleError))
	<div class="alert alert-danger" role="alert">
		{{$singleError}}
	</div>
@endif
<form class="form-horizontal" role="form" method="POST" action ="{{URL::to('account/changepassword')}}">	
	<div class="form-group">
		<label for="inputPW" class="col-sm-2 control-label">New password</label>
		<div class="col-sm-10">
		  <input class="form-control" type="password" id="inputPW" name="password" placeholder="password" value="">
		</div>
	</div>

	<div class="form-group">
		<label for="inputConfirmPW" class="col-sm-2 control-label">Confirm password</label>
		<div class="col-sm-10">
		  <input class="form-control" type="password" id="inputConfirmPW" name="password_confirmation" placeholder="Confirm password" value="">
		</div>
	</div>

	
	<hr/>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-10">		
			<button type="submit" class="btn btn-primary">Change password</button>			
		</div>
	</div>
</form>
@stop


{{-- Javascript --}}
@section('scripts')
<script src="{{asset('assets/js/bootstrap-datepicker.js')}}"></script>
@stop
