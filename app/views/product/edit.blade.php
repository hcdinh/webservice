@extends('layouts.default')

{{-- Header --}}
@section('header')
{{$header}}
@stop

{{-- Content --}}
@section('content')
@if (!isset($noProduct))
@if(isset($formErrors))
	<div class="alert alert-danger" role="alert">
		<ul>
			@foreach ($formErrors->all('<li>:message</li>') as $error)
		    {{$error}}
			@endforeach
		</ul>
	</div>
@endif
@if(isset($singleError))
	<div class="alert alert-danger" role="alert">
		{{$singleError}}
	</div>
@endif
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<form class="form-horizontal" role="form" method="POST" action ="{{$action}}"  enctype="multipart/form-data">
			<input type="hidden" name="type" value="{{$type}}"/>
			<div class="form-group">
				<label class="col-sm-3 control-label">Title</label>
				<div class="col-sm-9">
				  <input class="form-control" name="data[subtitle]" placeholder="Product name" value="@if(isset($product['subtitle'])){{$product['subtitle']}}@endif">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Summary</label>
				<div class="col-sm-9">
					<textarea name="data[summer]"  class="form-control" rows="3">@if(isset($product['summer'])){{$product['summer']}}@endif</textarea>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Author</label>
				<div class="col-sm-9">
				  <input class="form-control" name="data[author]" placeholder="Author" value="@if(isset($product['author'])){{$product['author']}}@endif">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Price</label>
				<div class="col-sm-9">
				  <input class="form-control" name="data[price]" placeholder="Price" value="@if(isset($product['price'])){{$product['price']}}@endif">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Category</label>
				<div class="col-sm-9">
					<select class="form-control" name="data[id_category]">
						<option>--- Please select a category ---</option>
						@foreach($categories as $category)
						<option value="{{$category->id_category}}" @if(isset($product['id_category']) && $product['id_category'] == $category->id_category) selected @endif>{{$category->category_name}}</option>
					  	@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Publisher</label>
				<div class="col-sm-9">
					<select class="form-control" name="data[id_publisher]">
						<option>--- Please select a publisher ---</option>
						@foreach($publishers as $publisher)
						<option value="{{$publisher->id_publisher}}" @if(isset($product['id_publisher']) && $product['id_publisher'] == $publisher->id_publisher) selected @endif>{{$publisher->publisher_name}}</option>
					  	@endforeach
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Special</label>
				<div class="col-sm-9">
					<select class="form-control" name="data[isSpecailproduct]">
						<option value="0" @if(isset($product['isSpecailproduct']) && $product['isSpecailproduct'] == 0) selected @endif>No</option>
						<option value="1" @if(isset($product['isSpecailproduct']) && $product['isSpecailproduct'] == 1) selected @endif>Yes</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Time for special</label>
				<div class="col-sm-9">
				  <input class="form-control" name="data[time_for_specail_product]" placeholder="Time for special" value="@if(isset($product['time_for_specail_product'])){{$product['time_for_specail_product']}}@endif">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Number of Payment</label>
				<div class="col-sm-9">
				  <input class="form-control" name="data[numPayment]" placeholder="Number of payment" value="@if(isset($product['numPayment'])){{$product['numPayment']}}@endif">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Status</label>
				<div class="col-sm-9">
					<select class="form-control" name="data[isEnable]">
						<option value="1" @if(isset($product['isEnable']) && $product['isEnable'] == 1) selected @endif>Enable</option>
					  	<option value="0" @if(isset($product['isEnable']) && $product['isEnable'] == 0) selected @endif>Disable</option>
					</select>
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Upload file epub</label>
				<div class="col-sm-9">
					<input type="file" name="file_epub">
					@if(isset($product['link_product_charge']))						
						<input type="hidden" name="old_file" value="{{$product['link_product_charge']}}" />
						<br/>
						<p><i>Current file: <a href="{{URL::to('product/download/'.$product['link_product_charge'])}}">{{$product['link_product_charge']}}</a></i></p>
					@endif
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Upload epub preview</label>
				<div class="col-sm-9">
					<input type="file" name="file_epub_demo">
					@if(isset($product['link_product_demo']))						
						<input type="hidden" name="old_file_demo" value="{{$product['link_product_demo']}}" />
						<br/>
						<p><i>Current file: <a href="{{URL::to('product/download/'.$product['link_product_demo'])}}">{{$product['link_product_demo']}}</a></i></p>
					@endif
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-3 control-label">Upload image</label>
				<div class="col-sm-9">
					<input type="file" name="file_image">
					@if(isset($product['link_image']))						
						<input type="hidden" name="old_file_image" value="{{$product['link_image']}}" />
						<br/>
						<img  class="img-thumbnail" src="{{URL::to('image/'.$product['link_image'])}}" alt="" />						
					@endif
				</div>
			</div>

			<hr/>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-9">
				@if($type == 'edit')
					<button type="submit" class="btn btn-primary">Update</button>
					<a href="{{URL::to('product')}}" class="btn btn-default">Cancel</a>
				@else
					<button type="submit" class="btn btn-primary">Create</button>
					<a href="{{URL::to('product')}}" class="btn btn-default">Return to list</a>
				@endif
				</div>
			</div>
		</form>
	</div>
</div>
@else
<div class="alert alert-danger" role="alert">Product isn't existed!</div>
@endif
@stop

