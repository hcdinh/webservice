@extends('layouts.default')

{{-- Header --}}
@section('header')
Delete product
@stop

{{-- Content --}}
@section('content')
<div class="panel panel-danger">
    <div class="panel-heading">
        Delete confirmation
    </div>
    <div class="panel-body">      
        <p>Product: {{$product->subtitle}}</p>
        <hr/>
        <p>Are you sure delete this product? </p>
      
    </div>
    <div class="panel-footer">
      <div class="pull-right">
          <a href="{{URL::to('product')}}" class="btn btn-default">Cancel</a>
          <a href="{{URL::to('product/doDelete/'.$product->id_product)}}" class="btn btn-primary">Delete</a>
      </div>
      <div class="clearfix">
      </div>
    </div>
</div>


@stop
