@extends('layouts.default')

{{-- Header --}}
@section('header')
Detail
@stop

{{-- Content --}}
@section('content')
@if (!isset($noProduct))
<div class="row">
	<div class="col-md-offset-2 col-md-8">
		<div class="panel panel-default">
			<div class="panel-heading"><h3 class="panel-title">Basic information</h3></div>
			<div class="panel-body">
				<div class="form-horizontal">
					<div class="form-group">
						<label class="col-sm-5">Title</label>
						<div class="col-sm-7">
						  @if(isset($product->subtitle)){{$product->subtitle}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Summary</label>
						<div class="col-sm-7">
							@if(isset($product->summer)){{$product->summer}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Author</label>
						<div class="col-sm-7">
						  @if(isset($product->author)){{$product->author}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Price</label>
						<div class="col-sm-7">
						  @if(isset($product->price)){{$product->price}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Category</label>
						<div class="col-sm-7">
							@if(isset($product->category->category_name)){{$product->category->category_name}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Publisher</label>
						<div class="col-sm-7">
							@if(isset($product->publisher->publisher_name)){{$product->publisher->publisher_name}}@endif						
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Special</label>
						<div class="col-sm-7">
							@if(isset($product->isSpecailproduct) && $product->isSpecailproduct == 1) Yes @endif
							@if(isset($product->isSpecailproduct) && $product->isSpecailproduct == 0) No @endif						
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Time for special</label>
						<div class="col-sm-7">
						  @if(isset($product->time_for_specail_product)){{$product->time_for_specail_product}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Number of payments</label>
						<div class="col-sm-7">
							@if(isset($product->numPayment)){{$product->numPayment}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Create date</label>
						<div class="col-sm-7">
							@if(isset($product->create_date)){{date('d-m-Y', strtotime($product->create_date))}}@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Uploader</label>
						<div class="col-sm-7">
							@if(isset($product->user->user_name)){{$product->user->user_name}}@endif						
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Status</label>
						<div class="col-sm-7">
							@if(isset($product->isEnable) && $product->isEnable == 1) Enable @endif
						  	@if(isset($product->isEnable) && $product->isEnable == 0) Disable @endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Epub file</label>
						<div class="col-sm-7">						
							@if(isset($product->link_product_charge))						
								<a href="{{URL::to('product/download/'.$product->link_product_charge)}}">{{$product->link_product_charge}}</a>
							@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Epub file demo</label>
						<div class="col-sm-7">						
							@if(isset($product->link_product_demo))						
								<a href="{{URL::to('product/download/'.$product->link_product_demo)}}">{{$product->link_product_demo}}</a>
							@endif
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-5">Epub image</label>
						<div class="col-sm-7">						
							@if(isset($product->link_image))						
								<img  class="img-thumbnail" src="{{URL::to('image/'.$product->link_image)}}" alt="" />						
							@endif
						</div>
					</div>
					<hr/>
					<div class="form-group">
						<div class="col-sm-offset-5 col-sm-7">
							<a href="{{URL::to('product/edit/'.$product->id_product)}}" class="btn btn-primary">Edit</a>
							<a href="{{URL::to('product/delete/'.$product->id_product)}}" class="btn btn-danger">Delete</a>
							<a href="{{URL::to('product')}}" class="btn btn-default">Return to list</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@else
<div class="alert alert-danger" role="alert">Product isn't existed!</div>
@endif

@stop

