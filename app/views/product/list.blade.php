@extends('layouts.default')

{{-- Header --}}
@section('header')
@if(isset($isSearch))
{{$isSearch}}
@else
Product Manager
<div class="pull-right">
<a href="{{URL::to('product/create')}}" class="btn btn-success">Create</a>
</div>
@endif
@stop

{{-- Content --}}
@section('content')
	<table class="table">
      <thead>
        <tr>
          <th>Image</th>
          <th>Title</th>
          <th>Category</th>
          <th>Publisher</th>
          <th>Author</th>
          <th>Price</th>
          <th>Special</th>
          <th>Status</th>
          <th>Uploader</th>
          <th>Create date</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
      	@foreach($products as $product)
        <tr>
          <td>
              @if(isset($product->link_image))
                <img  class="img-thumbnail" src="{{URL::to('image/'.$product->link_image)}}" alt="" width="100px" />
              @endif
          </td>
          <td>@if(isset($product->subtitle)){{$product->subtitle}}@endif</td>
          <td>@if(isset($product->category->category_name)){{$product->category->category_name}}@endif</td>
          <td>@if(isset($product->publisher->publisher_name)){{$product->publisher->publisher_name}}@endif</td>
          <td>@if(isset($product->author)){{$product->author}}@endif</td>
          <td>@if(isset($product->price)){{$product->price}}@endif</td>
          <td>
            @if(isset($product->isSpecailproduct) && $product->isSpecailproduct == 1) Yes @endif
            @if(isset($product->isSpecailproduct) && $product->isSpecailproduct == 0) No @endif
          </td>
          <td>
              @if(isset($product['isEnable']) && $product['isEnable'] == 1) Enable @endif
              @if(isset($product['isEnable']) && $product['isEnable'] == 0) Disable @endif
          </td>
          <td>@if(isset($product->id_user)){{$product->id_user}}@endif</td>
          <td>@if(isset($product->create_date)){{date('d-m-Y', strtotime($product->create_date))}}@endif</td>
          <td>
          	<a href="{{URL::to('product/edit/'.$product->id_product)}}" class="btn btn-primary btn-xs">Edit</a>
          	<a href="{{URL::to('product/delete/'.$product->id_product)}}" class="btn btn-danger btn-xs">Delete</a>
            <a href="{{URL::to('product/detail/'.$product->id_product)}}" class="btn btn-default btn-xs">Detail</a>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {{$products->links()}}

@stop

