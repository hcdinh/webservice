@extends('layouts.default')

{{-- Header --}}
@section('header')
@if(isset($isSearch))
{{$isSearch}}
@else
Payment List
@endif
@stop

{{-- Content --}}
@section('content')
<table class="table">
  <thead>
    <tr>
      <th>Email</th>
      <th>Product</th>
      <th>Price</th>
      <th>Create Date</th>
      <th>Promotion</th>
      <th>Download</th>
    </tr>
  </thead>
  <tbody>
    @foreach($payments as $payment)
      <tr>        
        <td>@if(isset($payment->user->email)){{$payment->user->email}}@endif</td>
        <td>@if(isset($payment->product->subtitle)){{$payment->product->subtitle}}@endif</td>
        <td>@if(isset($payment->price)){{$payment->price}}@endif</td>
        <td>@if(isset($payment->create_date)){{date('d-m-Y', strtotime($payment->create_date))}}@endif</td>
        <td>
          @if(isset($payment->isPromotion) && $payment->isPromotion == 0) No @endif
          @if(isset($payment->isPromotion) && $payment->isPromotion == 1) Yes @endif
        </td>
        <td>
          @if(isset($payment->canDownload) && $payment->canDownload == 0) No @endif
          @if(isset($payment->canDownload) && $payment->canDownload == 1) Yes @endif
        </td>
      </tr>      
      @endforeach
  </tbody>
</table>
{{$payments->links()}}

@stop
