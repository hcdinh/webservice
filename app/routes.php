<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::any('/', 'GeneralController@login');
Route::any('/login', 'GeneralController@login');
Route::get('/image/{image?}/{width?}/{height?}/{type?}', 'GeneralController@image');

Route::get('/profile', 'AccountController@profile');
Route::get('/logout', 'AccountController@logout');
Route::post('account/changepassword', 'AccountController@changePassword');

Route::get('/user', 'UserController@index');
Route::any('/user/edit/{id?}/', 'UserController@edit');
Route::any('/user/create', 'UserController@create');
Route::get('/user/delete/{id?}/', 'UserController@delete');
Route::get('/user/doDelete/{id?}/', 'UserController@doDelete');
Route::get('/user/detail/{id?}/', 'UserController@detail');
Route::any('/user/search', 'UserController@search');
Route::get('/user/deletedevice/{id?}/{userId?}', 'UserController@delDevice');


Route::get('/category', 'CategoryController@index');
Route::any('/category/create', 'CategoryController@create');
Route::any('/category/edit/{id?}', 'CategoryController@edit');
Route::get('/category/delete/{id?}', 'CategoryController@delete');
Route::get('/category/doDelete/{id?}', 'CategoryController@doDelete');
Route::any('/category/search', 'CategoryController@search');


Route::get('/publisher', 'PublisherController@index');
Route::any('/publisher/create', 'PublisherController@create');
Route::any('/publisher/edit/{id?}', 'PublisherController@edit');
Route::get('/publisher/delete/{id?}', 'PublisherController@delete');
Route::get('/publisher/doDelete/{id?}', 'PublisherController@doDelete');
Route::any('/publisher/search', 'PublisherController@search');

Route::get('/product', 'ProductController@index');
Route::any('/product/create', 'ProductController@create');
Route::any('/product/edit/{id?}', 'ProductController@edit');
Route::get('/product/delete/{id?}', 'ProductController@delete');
Route::get('/product/doDelete/{id?}', 'ProductController@doDelete');
Route::any('/product/search', 'ProductController@search');
Route::any('/product/detail/{id?}', 'ProductController@detail');
Route::any('/product/download/{filename?}', 'ProductController@download');

Route::get('/special', 'SpecialController@index');
Route::post('/special/change', 'SpecialController@change');

Route::get('/payment', 'PaymentController@index');
Route::any('/payment/search', 'PaymentController@search');

Route::post('/api/v1/signin', 'APIController@signin');
Route::post('/api/v1/login', 'APIController@login');
Route::post('/api/v1/logout', 'APIController@logout');
Route::post('/api/v1/update', 'APIController@updatePassword');
Route::post('/api/v1/check_login', 'APIController@checkLogin');
Route::post('/api/v1/get_info_product', 'APIController@getInfoProduct');
Route::post('/api/v1/get_productlist1', 'APIController@getListProduct1');
Route::post('/api/v1/get_productlist2', 'APIController@getListProduct2');
Route::post('/api/v1/check_product_payed', 'APIController@checkBuyProduct');
Route::post('/api/v1/check_expired_product', 'APIController@checkExpiredProduct');
Route::post('/api/v1/pay_product', 'APIController@buyProduct');
Route::post('/api/v1/get_product', 'APIController@downloadEpub');
Route::get('/api/v1/get_product', 'APIController@downloadEpubDirect');
Route::post('/api/v1/get_devicelist', 'APIController@getListDevice');
Route::post('/api/v1/delete_device', 'APIController@deleteDevice');
Route::post('/api/v1/get_paymentlist', 'APIController@getListPayment');


Route::any('/test', function () {
	echo CommonHelper::decrypt('cVdulL00j66vLB0WcI6epw==');
	return View::make('test');
});