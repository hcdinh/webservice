<?php

return array(

	'epub_folder' => __DIR__."/../../epub/",
	'image_folder' => __DIR__."/../../assets/upload/",
	'number_chapter_preview' => 3,
	'number_character_preview' => 2000,
	'pre_text_file' => 'preview_',
	'img_text_file' => 'img_',
	'time_session' => 1440,

);
