<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'en' => array(
		'json_format' => "Data doesn't have json format",
		'no_data' => 'No data',
		'email_existed' => 'Email existed in database!',
		'full_device' => 'You used maximum three other devices to login, so you can\'t use this device to login. Please login with your used devices.',
		'login_fail' => 'Your username or password is wrong!',
		'no_token' => 'This API required login',
		'no_device' => 'Device name is required',
		'wrong_token' => 'You are logged out. Please log in again.',
		'session_timeout' => 'You are logged out. Please log in again.',
		'wrong_old_pass' => 'Old password is wrong',
		'no_product' => 'Product isn\'t exist',
	),


	'vi' => array(
		'json_format' => "Dữ liệu không có định dạng JSON",
		'no_data' => 'Không có dữ liệu',
		'email_existed' => 'Email đã tồn tại, vui lòng sử dụng email khác.',
		'full_device' => 'Bạn đã dùng 3 thiết bị để đăng nhập, vì thế không thể dùng thiết bị này để đăng nhập. vui lòng đăng nhập bằng thiết bị cũ.',
		'login_fail' => 'Username hoặc password nhập chưa đúng!',
		'no_token' => 'Vui lòng cung cấp token',
		'no_device' => 'Vui lòng cung cấp tên thiết bị',
		'wrong_token' => 'Bạn đã bị thoát, vui lòng đăng nhập lại.',
		'session_timeout' => 'Bạn đã bị thoát, vui lòng đăng nhập lại.',
		'wrong_old_pass' => 'password cũ nhập sai.',
		'no_product' => 'Sản phẩm không tồn tại',
	),

	'field_en' => array (
		'user_name' => 'Username',
		'bored_date' => 'Birthday',
		'sex' => 'Sex',
		'oldpass' => 'Old password',
		'newpass' => 'New password',
	),

	'field_vi' => array (
		'user_name' => 'Họ & Tên',
		'bored_date' => 'Ngày sinh',
		'sex' => 'Giới tính',
		'oldpass' => 'Password cũ',
		'newpass' => 'Password mới',
	),


);
