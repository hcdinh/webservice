<?php

class CommonHelper
{

	public static function getCurrentAction()
	{
		$route = Route::currentRouteAction();
		$data = explode("@", $route);
		return $data[1];
	}

	public static function isAdmin()
	{
		$user = Session::get('user');
		if($user['permission'] == 1)
			return TRUE;
		return FALSE;
	}

	/**
	 * resize image
	 * @param filename string
	 * @param width
	 * @param height
	 * @param type char [default, w, h]
	 *				default = scale with white space,
	 *				w = fill according to width,
	 *				h = fill according to height
	 * @return [type]           [description]
	 */
	public static function resizeImage($filename, $width, $height, $type = "")
	{
		$dirImage = CommonHelper::normalizePath(Config::get('webservice.image_folder'));

		if (!file_exists($dirImage . $filename) || !is_file($dirImage . $filename)) {
			echo $dirImage . $filename;
			return;
		}

		$info = pathinfo($filename);

		$extension = $info['extension'];

		$old_image = $filename;
		$new_image = "image/" . substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . $type .'.' . $extension;

		if (!file_exists($dirImage . $new_image) || (filemtime($dirImage . $old_image) > filemtime($dirImage . $new_image))) {
			$path = '';

			$directories = explode('/', dirname(str_replace('../', '', $new_image)));

			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;

				if (!file_exists($dirImage . $path)) {
					@mkdir($dirImage . $path, 0777);
				}
			}

			list($width_orig, $height_orig) = getimagesize($dirImage . $old_image);

			if ($width_orig != $width || $height_orig != $height) {
				$image = new Image($dirImage . $old_image);
				$image->resize($width, $height, $type);
				$image->save($dirImage . $new_image);
			} else {
				copy($dirImage . $old_image, $dirImage . $new_image);
			}
		}
		return $new_image;
	}

	public static function normalizePath($path)
	{
	    $parts = array();// Array to build a new path from the good parts
	    $path = str_replace('\\', '/', $path);// Replace backslashes with forwardslashes
	    $path = preg_replace('/\/+/', '/', $path);// Combine multiple slashes into a single slash
	    $segments = explode('/', $path);// Collect path segments
	    $test = '';// Initialize testing variable
	    foreach($segments as $segment)
	    {
	        if($segment != '.')
	        {
	            $test = array_pop($parts);
	            if(is_null($test))
	                $parts[] = $segment;
	            else if($segment == '..')
	            {
	                if($test == '..')
	                    $parts[] = $test;

	                if($test == '..' || $test == '')
	                    $parts[] = $segment;
	            }
	            else
	            {
	                $parts[] = $test;
	                $parts[] = $segment;
	            }
	        }
	    }
	    return implode('/', $parts);
	}


	public static function validateDataAPI($data, $rule)
	{
		$msg = array();
		//English language
		App::setLocale('en');
		$validator = Validator::make($data, $rule);
		$fieldnameEn = Config::get('message.field_en');
		$validator->setAttributeNames($fieldnameEn);
		if ($validator->fails())
        {
            $msg['en'] = CommonHelper::getStringError($validator);
        }
        else
        {
        	return 1;
        }

        //Vietnamese language
		App::setLocale('vi');		
		$validator = Validator::make($data, $rule);
		$fieldnameVi = Config::get('message.field_vi');
		$validator->setAttributeNames($fieldnameVi);
        $msg['vi'] = CommonHelper::getStringError($validator);
        return $msg;
	}

	public static function getStringError($validator)
	{
		$messages = $validator->messages()->toArray();
		$rs = array();
		foreach ($messages as $msg)
		{
			$rs[]= $msg[0];
		}
		return implode("||", $rs);
	}

	public static function encrypt($text)
	{
		$key = Config::get('app.key');
		$aes = new AES($text, $key, 128);
		return $aes->encrypt();
	}

	public static function decrypt($text)
	{
		$key = Config::get('app.key');
		$aes = new AES($text, $key, 128);		
		return $aes->decrypt();
	}
}