<?php

class EpubHelper {

	public static $_instance = null;

	protected $_file;

	public function __construct($epubFile)
	{
		$this->_file = $epubFile;
	}

	public static function initInstance($epubFile)
	{
		if(is_null(self::$_instance))
		{
			self::$_instance = new EpubHelper($epubFile);
		}
		return self::$_instance;
	}

	public function checkDRM()
	{
		if(empty($this->_file))
		{
			return null;
		}		
		$epubPath = CommonHelper::normalizePath(Config::get('webservice.epub_folder'));
		$tempPath = $epubPath."temp/";
		if (!file_exists($tempPath)) 
		{
		    mkdir($tempPath, 0777, true);
		}		
		$zip = new ZipArchive();
		$rs = FALSE;
		if($zip->open($this->_file) === TRUE)
		{			
			//Extract to temp folder
			$zip->extractTo($tempPath);
			$zip->close();	

			$encryptionXmlPath = $tempPath."/META-INF/encryption.xml";
			if(file_exists($encryptionXmlPath))
			{				
				$rs = TRUE;
			}			
		}
		$zipEpub = new ZipHelper;
		$zipEpub->rrmdir($tempPath);
		return $rs;
	}

	public function getPreviewEpub()
	{
		if(empty($this->_file))
		{
			return null;
		}
		$epubPath = CommonHelper::normalizePath(Config::get('webservice.epub_folder'));
		$tempPath = $epubPath."temp/";
		if (!file_exists($tempPath)) 
		{
		    mkdir($tempPath, 0777, true);
		}

		$zip = new ZipArchive();
		if($zip->open($this->_file) === TRUE)
		{
			//Extract to temp folder
			$zip->extractTo($tempPath);
			$zip->close();	

			//Path to container.xml
			$containerXmlPath = $tempPath."/META-INF/container.xml";

			//Read and get file opf
			$dataContainerXML = simplexml_load_file($containerXmlPath);
			$opfPath = $tempPath.$dataContainerXML->rootfiles->rootfile['full-path'];

			//Read OPF path
			$dom = new DOMDocument;
			$dom->load($opfPath);
			$dataOPFXML = $dom->documentElement;

			//Array delete file
			$arrDelete = array();
			$arrToRemove = array();
			$i = 0;
			$numChapterPreview = Config::get('webservice.number_chapter_preview');
			$numCharacterOnPage = Config::get('webservice.number_character_preview');
			$itemrefs = $dataOPFXML->getElementsByTagName("itemref");	
			$arrToCutPage = array();		

			foreach ($itemrefs as $itemref) 
			{
				$i++;
				if($i<=$numChapterPreview)
				{	
					$arrToCutPage[] = $itemref->getAttribute('idref');
					continue;

				}
				else
				{					
					$arrDelete[] = $itemref->getAttribute('idref');
					$arrToRemove[] = $itemref;					
				}				
			}

			//Real Delete
			foreach ($arrToRemove as $removeNode) 
			{
				$removeNode->parentNode->removeChild($removeNode);
			}

			//Delete manifest
			$items = $dataOPFXML->getElementsByTagName("item");
			$arrToRemove = array();
			$arrDelFile = array();			
			foreach ($items as $item) 
			{
				$idTemp = $item->getAttribute('id');
				if(in_array($idTemp,$arrDelete))
				{					
					$arrToRemove[] = $item;	
					$arrDelFile[] = $item->getAttribute('href');
				}
				/*elseif(in_array($idTemp, $arrToCutPage))
				{
					//Cut page
					$linkfile = dirname($opfPath)."/".$item->getAttribute('href');
					$htmlSource = file_get_contents($linkfile);
					preg_match_all("/(.*)<body.*?>(.*)<\/body>(.*)/is", $htmlSource, $matches);
					if(count($matches) == 4 && isset($matches[1][0]) && isset($matches[2][0]) && isset($matches[3][0]))
					{						
						$top = $matches[1][0];
						$contents = $matches[2][0];
						$bottom = $matches[3][0];										
						
						$contents = str_replace("epub:","mycodeepub",$contents);
						$contents = str_replace("ssml:","mycodessml",$contents);
						$contents = html_entity_decode($contents);
						$contents = preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $contents);
						try
						{
							$cutter = new HtmlCutter($contents, $numCharacterOnPage);
						}
						catch(Exception $ex)
						{
							print_r($ex->getMessage());
							echo $contents;
							die();
						}
						$contents = $cutter->cut();
						$contents = str_replace("mycodeepub","epub:",$contents);
						$contents = str_replace("mycodessml","ssml",$contents);
													
						file_put_contents($linkfile,$top."<body>".$contents."</body>".$bottom);
						unset($contents);	
					}
					
				}*/

			}

			//Real Delete
			foreach ($arrToRemove as $removeNode) 
			{
				$removeNode->parentNode->removeChild($removeNode);
			}

			//Remove file
			foreach ($arrDelFile as  $file) 
			{
				if(file_exists(dirname($opfPath)."/".$file))
				{
					unlink(dirname($opfPath)."/".$file);	
				}				
			}

			//Write to opf file
			file_put_contents($opfPath,$dom->saveXML());	

			//Zip file to epub
			$preText = Config::get('webservice.pre_text_file');
			$zipname = $epubPath.'/'.$preText.basename($this->_file);
		    $zipEpub = new ZipHelper;		    
		    if($zipEpub->open($zipname, ZipArchive::CREATE) === TRUE) 
			{
			    $zipEpub->addDirDo($tempPath, basename($tempPath));			    
			}
			$zipEpub->close();
		    //Delete temp folder
		    $zipEpub->rrmdir($tempPath);
			
			return $zipname;
		}
		return null;
	}
}