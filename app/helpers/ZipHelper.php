<?php
class ZipHelper extends ZipArchive 
{    

    public function addDir($location, $name) 
    {
        $this->addEmptyDir($name);
        $this->addDirDo($location, $name);
    } 
    
    public function addDirDo($location, $name) 
    {
        $name .= '/';
        $location .= '/';
        
        $dir = opendir ($location);
        while ($file = readdir($dir))
        {
            if ($file == '.' || $file == '..') continue;            
            $do = (filetype( $location . $file) == 'dir') ? 'addDir' : 'addFile';
            $this->$do($location . $file, $name . $file);
        }
    }

    public function rrmdir($dir) 
    {
       if (is_dir($dir)) 
       {
         $objects = scandir($dir);
         foreach ($objects as $object) 
         {
           if ($object != "." && $object != "..") 
           {
             if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); 
             else unlink($dir."/".$object);
           }
         }
         reset($objects);
         rmdir($dir);
       }
    }
}